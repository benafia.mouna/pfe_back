import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class CommandeService {
  
  private apiServeurUrl = environment.apiBaseUrl;
  constructor(private http: HttpClient,public auth: AuthService) { }

  public getCommande() { 
    return this.http.get(`${this.apiServeurUrl}/commande/all`);
  }
  public getCommandeBy(id:any) { 
    return this.http.get(`${this.apiServeurUrl}/commande/findby?id=`+id);
  } 
 
  public addCommande(iduser : any,idcountry : any,ids: any,commande: any) {
    return this.http.post(`${this.apiServeurUrl}/commande/add/${iduser}/${idcountry}?ids=${ids}`, commande);
  }
  public updateCommande(commande: any , idpanier : any, id : any){
    return this.http.put(`${this.apiServeurUrl}/commande/update/${idpanier}/${id}`, commande);
  }

  public deleteCommande(commandeId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServeurUrl}/commande/delete/${commandeId}`);
  }

}

