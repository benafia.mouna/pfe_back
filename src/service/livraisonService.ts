import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class LivraisonService {
   private apiServeurUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getLivraison() { 
    return this.http.get(`${this.apiServeurUrl}/livraison/all`);
  }
  public getCommandevendeur(iduser : any){

    console.log("url of ",`${this.apiServeurUrl}/promotion/findpromouser/${iduser}`);

    return this.http.get(`${this.apiServeurUrl}/promotion/findpromouser/${iduser}`)
  }

  public addLivraison(livraison: any , idmodelivraison : any , idcommande : any) {
    return this.http.post(`${this.apiServeurUrl}/livraison/add/${idmodelivraison}/${idcommande}`, livraison);
  }
  public updateLivraison(livraison: any,idmodelivraison:any ,idcommande : any , id:any){
    return this.http.put(`${this.apiServeurUrl}/livraison/update/${idmodelivraison}/${idcommande}/${id}`, livraison);
  }


  public deleteLivraison(livraisonId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServeurUrl}/livraison/delete/${livraisonId}`);
  }
  

}

