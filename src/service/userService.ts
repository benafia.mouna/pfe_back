import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../model/user';
import { environment } from 'src/environments/environment';
import { TokenInterceptorService } from './tokenInterceptorService';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class UserService {
   private apiServeurUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getUsers(): Observable<User[]> { 
    return this.http.get<any>(`${this.apiServeurUrl}/user/all`);

  }

  //nofileemodifimage

  public noupdateUser(user: any  , iduser:any): Observable<User> {
    return this.http.put<User>(`${this.apiServeurUrl}/user/noupdate/${iduser}`, user);
  } 
  public findDiscount(id:any){
    return this.http.get(`${this.apiServeurUrl}/discount/find/${id}`,{ responseType: 'text'})
  }
  public updateUser(user: any  , iduser:any): Observable<User> {
    return this.http.put<User>(`${this.apiServeurUrl}/user/update/${iduser}`, user);
  }
  public updatePassword(id:any,newpassword:any) {
    return this.http.put(`${this.apiServeurUrl}/user/updatepassword/${id}?newpassword=${newpassword}`, {});
  }
  public findUser(email:any) {
    return this.http.get<User>(`${this.apiServeurUrl}/user/findUserEmail1/${email}`);
  }
  public updatepourcentage(discount:any){
    return this.http.put(`${this.apiServeurUrl}/discount/update/1`, discount);
  }
  public scraping(){
    return this.http.get(`http://127.0.0.1:5000/`, {})

  }
  public deleteUser(userId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServeurUrl}/user/delete/${userId}`);
  }
 
}
