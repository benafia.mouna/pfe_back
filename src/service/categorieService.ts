import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class CategorieService {
   private apiServeurUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getCategorie() { 
    return this.http.get(`${this.apiServeurUrl}/categorie/all`);
  }

  public addCategorie(categorie: any) {
    return this.http.post(`${this.apiServeurUrl}/categorie/add`, categorie);
  }
  public updateCategorie(categorie: any,id:any){
    return this.http.put(`${this.apiServeurUrl}/categorie/update/${id}`, categorie);
  }
  public getOneCategorie(id:any) {
    return this.http.get(`${this.apiServeurUrl}/subcategorie/findName/${id}`,{ responseType: 'text'})
  }


  public deleteCategorie(categorieId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServeurUrl}/categorie/delete/${categorieId}`);
  }

}

