import {Injectable , Injector} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from './authService';
import {FormGroup} from '@angular/forms';

@Injectable({ providedIn: 'root'})

export class TokenInterceptorService {
  public loginForm: FormGroup;
  constructor(public auth: AuthService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
     if (request.url === 'http://localhost:8090/api/v1/registration' || request.url.includes('/login') || request.url.includes('findUserEmail/')|| request.url.includes('verifPassword')|| request.url.includes('forgetpassword')|| request.url.includes('savePassword') ){
      return next.handle(request);
    }
    //console.log("ici test ===" + this.auth.GetToken());
    
    let str = this.auth.GetToken();
    str = str. substring(1);
    str = str.slice(0, -1);
        request = request.clone({
       setHeaders: {
         Authorization: `Bearer ${str}`,
       }
    });
    
     return next.handle(request);
  }
}

