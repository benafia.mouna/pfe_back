import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class MarqueService {
   private apiServeurUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getMarque() { 
    return this.http.get(`${this.apiServeurUrl}/marque/all`);
  }

  public addMarque(marque: any) {
    return this.http.post(`${this.apiServeurUrl}/marque/add/`, marque);
    
  }

  public modifimage(marque: any, id:any){
    return this.http.put(`${this.apiServeurUrl}/marque/modifimage/${id}`, marque);
  }
  public modifnoimage(marque: any, id:any){
    return this.http.put(`${this.apiServeurUrl}/marque/modifnoimage/${id}`, marque);
  }
  public deleteMarque(marqueId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServeurUrl}/marque/delete/${marqueId}`);
  }

}

