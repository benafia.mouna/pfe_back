import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class PromoService {
   private apiServeurUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getPromo() { 
    return this.http.get(`${this.apiServeurUrl}/promotion/all`);
  }

  public getPromoBy(id:any) { 
    return this.http.get(`${this.apiServeurUrl}/promotion/findby?id=`+id);
  } 
  public getOnePromo(id:any) {
    return this.http.get(`${this.apiServeurUrl}/promotion/find/${id}`)
  }
 
  public getCommandevendeur(iduser : any){

    console.log("url of ",`${this.apiServeurUrl}/promotion/findpromouser/${iduser}`);

    return this.http.get(`${this.apiServeurUrl}/promotion/findpromouser/${iduser}`)
  }

  public addPromo(promotion: any  , idsubdomaine :any , iduser : any ) {
    return this.http.post(`${this.apiServeurUrl}/promotion/add/${idsubdomaine}/${iduser}`, promotion);
  }
  public updatePromowithFile(promotion: any , id:any){
    return this.http.put(`${this.apiServeurUrl}/promotion/updatewithfile/${id}`, promotion);
  }
  public updatenofile(promotion: any , id:any){
    return this.http.put(`${this.apiServeurUrl}/promotion/updatenofile/${id}`, promotion);
  }

  public updatennotif(promotion: any , id:any){
    return this.http.put(`${this.apiServeurUrl}/promotion/updatennotif/${id}`, promotion);
  } 
  public noupdatennotif(promotion: any , id:any){
    return this.http.put(`${this.apiServeurUrl}/promotion/updatennotif/${id}`, promotion);
  } 


  public updatePrix(idpromo : any , price : any , rem: any){
    return this.http.put(`${this.apiServeurUrl}/promotion/update/${idpromo}/${price}/${rem}`,{})
  }
  public updatestatusconfirm(idpromo : any ){
    return this.http.put(`${this.apiServeurUrl}/promotion/confirmstatus/${idpromo}`,{})
  }
  public updatestatusnotconfirm(idpromo : any ){
    return this.http.put(`${this.apiServeurUrl}/promotion/notconfirmstatus/${idpromo}`,{})
  }

  public deletePromo(id: any , ids:any ) {
    return this.http.post(`${this.apiServeurUrl}/promotion/delete/${id}?ids=${ids}`,{});
  }

}

