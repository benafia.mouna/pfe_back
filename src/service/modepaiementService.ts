import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class ModePaiementService {
   private apiServeurUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getModePaiement() { 
    return this.http.get(`${this.apiServeurUrl}/modepaiement/all`);
  }

  public addModePaiement(modepaiement: any) {
    return this.http.post(`${this.apiServeurUrl}/modepaiement/add`, modepaiement);
  }
  public updateModePaiement(modepaiement: any , id:any){
    return this.http.put(`${this.apiServeurUrl}/modepaiement/update/${id}`, modepaiement);
  }

  public deleteModePaiement(modepaiementId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServeurUrl}/modepaiement/delete/${modepaiementId}`);
  }

}

