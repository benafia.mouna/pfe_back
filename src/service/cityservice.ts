import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class CityService {
  private apiServeurUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getCity() { 
    return this.http.get(`${this.apiServeurUrl}/city/all`);
  }
  public addCity(city: any , idcountry : any) {
    console.log(`${this.apiServeurUrl}/city/add/${idcountry}`, city)
    return this.http.post(`${this.apiServeurUrl}/city/add/${idcountry}`, city);
  }
  public updateCity(city: any, idcountry:any, id:any ){
    return this.http.put(`${this.apiServeurUrl}/city/update/${idcountry}/${id}/`, city);
  }

  public deleteCity(cityId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServeurUrl}/city/delete/${cityId}`);
  }

}