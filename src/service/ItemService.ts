import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class ItemService {
   private apiServeurUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getItem() { 
    return this.http.get(`${this.apiServeurUrl}/item/all`);
  }

  public addItem(item: any , idmarque : any, idpromo : any) {
    return this.http.post(`${this.apiServeurUrl}/item/add/${idmarque}/${idpromo}`, item);
  }
  public AVG_PRICE(ProduitName: any, Categorie: any)
  {
    //ProduitName = 'pc lenovo'
    //Categorie = 'anniversaire'
    return this.http.get(`http://127.0.0.1:5000/insert?Categorie=${Categorie}&name=${ProduitName}`,{ responseType: 'text'})
    
  }
  public updateItem(item: any, idmarque : any , idpromo : any , id : any){
    return this.http.put(`${this.apiServeurUrl}/item/update/${idmarque}/${idpromo}/${id}`, item);
  }
  public findDiscount(id:any){
    return this.http.get(`${this.apiServeurUrl}/discount/find/${id}`,{ responseType: 'text'})
  }
  public noupdateItem(item: any, idmarque : any , idpromo : any , id : any){
    return this.http.put(`${this.apiServeurUrl}/item/noupdateupdate/${idmarque}/${idpromo}/${id}`, item);
  }
  

  public deleteItem(itemId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServeurUrl}/item/delete/${itemId}`);
  }

}

