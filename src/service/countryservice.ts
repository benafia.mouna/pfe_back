import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class CountryService {
  private apiServeurUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getCountry() { 
    return this.http.get(`${this.apiServeurUrl}/country/all`);
  }
  public addCountry(country: any ) {
    console.log(`${this.apiServeurUrl}/country/add/`, country)
    return this.http.post(`${this.apiServeurUrl}/country/add/`, country);
  }
  public updateCountry(country: any,id:any){
    return this.http.put(`${this.apiServeurUrl}/country/update/${id}`, country);
  }

  public deleteCountry(countryId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServeurUrl}/country/delete/${countryId}`);
  }

}