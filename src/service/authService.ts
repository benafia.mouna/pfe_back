import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import {User} from '../model/user';
import {Observable} from 'rxjs';


@Injectable({ providedIn: 'root'})

export class AuthService {
  constructor(private http: HttpClient) {
  }

  login(user: User) {
    return this.http.post('http://localhost:8090/login', user,  { responseType: 'text'} );
  }
  public findUser(email:any) {
    return this.http.get<User>(`http://localhost:8090/user/findUserEmail1/${email}`);
  }
  public findUserByEmail(email: string): Observable<boolean> {
    return this.http.get<boolean>(`http://localhost:8090/user/findUserEmail/${email}`);
  }
  register(user: any) {
    return this.http.post('http://localhost:8090/api/v1/registration', user ,  { responseType: 'text'});

  }
  VerificationMDP(user: User){
    return this.http.post('http://localhost:8090/user/verifPassword', user )
  }
 IsLoggedIn(){
    return localStorage.getItem('token') != null;
 }
 GetToken(){
    return localStorage.getItem('token');
 }
 
 forgetpassword(email: any){
   return this.http.post(`http://localhost:8090/user/forgetpassword?email=${email}`,{})

 }
 savePassword(resetLink: string , newPassword : string){
  return this.http.post(`http://localhost:8090/user/savePassword/${resetLink}?newPassword=${newPassword}`, {});
 }
 
}
