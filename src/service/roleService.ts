import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class RoleService {


  
   private apiServeurUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getRole() { 
    return this.http.get(`${this.apiServeurUrl}/role/all`);
  }


  public deleteRole(roleId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServeurUrl}/role/delete/${roleId}`);
  }
  public addpermission(name : string , ids:any){
    return this.http.put(`${this.apiServeurUrl}/role/saverole/${name}?ids=${ids}`, {}  );
  }
  public removepermission(name : string , ids:any) {
    return this.http.post(`${this.apiServeurUrl}/role/removeFromRole/${name}?ids=${ids}`, {} );
  }
}

