import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class PermissionService {
   private apiServeurUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getPermission() { 
    return this.http.get(`${this.apiServeurUrl}/permission/all`);
  }

  public addPermission(permission:any) {
    return this.http.post(`${this.apiServeurUrl}/permission/add/`, permission);
  }
  public updatePermission(permission: any , id : any){
    return this.http.put(`${this.apiServeurUrl}/permission/update/${id}`, permission);
  }

  public deletePermission(PermissionId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServeurUrl}/permission/delete/${PermissionId}`);
  }

}

