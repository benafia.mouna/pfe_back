import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { Mail } from "src/model/mail";
import { AuthService } from "./authService";

@Injectable({ providedIn: 'root'})
export class MailService {
    private apiServeurUrl = environment.apiBaseUrl;
    constructor(private http: HttpClient,public auth: AuthService) { }

    sendmail(mail: any) {
      // const m: Mail = {
      //   subject:"this.addForm.value.subject",
      //   content: "this.addForm.value.content",
      //   to: "bensalemarwen@gmail.com",
      //   from:"genova.angel@hotmail.com"
      //         };
        return this.http.post('http://localhost:8090/sendmail/',mail);
    
      }
}