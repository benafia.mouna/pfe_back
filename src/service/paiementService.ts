import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class PaiementService {
   private apiServeurUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getPaiement() { 
    return this.http.get(`${this.apiServeurUrl}/paiement/all`);
  }

  public addPaiement(paiement: any , idcommande : any , idmodepaiement : any) {
    return this.http.post(`${this.apiServeurUrl}/paiement/add/${idcommande}/${idmodepaiement}`, paiement);
  }
  public updatePaiement(paiement: any ,idcommande : any , idmodepaiement : any , id : any){
    console.log('my url = ', `${this.apiServeurUrl}/paiement/update/${id}/${idcommande}/${idmodepaiement}`)
    return this.http.put(`${this.apiServeurUrl}/paiement/update/${id}/${idcommande}/${idmodepaiement}`, paiement);

  }

  public deletePaiement(PaiementId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServeurUrl}/paiement/delete/${PaiementId}`);
  }

}

