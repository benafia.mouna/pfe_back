import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class ModeLivraisonService {
   private apiServeurUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getModeLivraison() { 
    return this.http.get(`${this.apiServeurUrl}/modelivraison/all`);
  }

  public addModeLivraison(modelivraison: any ) {
    return this.http.post(`${this.apiServeurUrl}/modelivraison/add/`, modelivraison);
  }
  public updateModeLivraison(modelivraison: any ,id : any){
    return this.http.put(`${this.apiServeurUrl}/modelivraison/update/${id}/`, modelivraison);
  }


  public deleteModeLivraison(modelivraisonId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServeurUrl}/modelivraison/delete/${modelivraisonId}`);
  }

}

