export class Mail {
    from?: string;
    to?: string;
    subject?: string;
    content?: string;
    
}