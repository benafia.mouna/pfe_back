import { HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { DesignutilityServiceService } from "./designutility-service.service";


@Injectable()
export class LoaderIntercept implements HttpInterceptor {
    constructor(private _du : DesignutilityServiceService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            tap(event => {
                this._du.loader.next(true)
                if (event.type == HttpEventType.Response) {
                    if (event.status == 100) {
                        this._du.loader.next(false)
                    }
                }
            })
        )
    }
}