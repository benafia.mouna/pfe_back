import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DesignutilityServiceService {

  constructor(private _httpClient : HttpClient) { }
loader = new BehaviorSubject<Boolean>(true);
  getPosts():Observable<any> {
    return this._httpClient.get("https://jsonplaceholder.typicode.com/posts");
  }
  getComment():Observable<any>{
    return this._httpClient.get("https://jsonplaceholder.typicode.com/comments")
  }

}
