import { TestBed } from '@angular/core/testing';

import { DesignutilityServiceService } from './designutility-service.service';

describe('DesignutilityServiceService', () => {
  let service: DesignutilityServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DesignutilityServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
