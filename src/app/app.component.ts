import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {

  event$;


  title = 'multikart-backend';

  constructor(private router:Router){
    this.event$ = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationStart) {
        console.log(event.url);
        if (localStorage.getItem("state") === "1" && event.url == "/auth/login" || event.url == "/login" && localStorage.getItem("state") === "1" || event.url == "/" && localStorage.getItem("state") === "1"  ) {

            this.router.navigate(['dashboard/default']);


          
        }
      }
    });

  }
}
