import { Routes } from '@angular/router';

export const content: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('../../components/dashboard/dashboard.module').then(m => m.DashboardModule),
  },
  {
    path: 'promotions',
    loadChildren: () => import('../../components/products/products.module').then(m => m.ProductsModule),
    data: {
      breadcrumb: "Promotions"
    }
  },
  {
    path: 'orders',
    loadChildren: () => import('../../components/sales/sales.module').then(m => m.SalesModule),
    data: {
      breadcrumb: "Orders"
    }
  },
  
  {
    path: 'users',
    loadChildren: () => import('../../components/users/users.module').then(m => m.UsersModule),
    data: {
      breadcrumb: "Users"
    }
  },
  {
    path: 'livraison',
    loadChildren: () => import('../../components/livraisons/livraisons.module').then(m => m.LivraisonsModule),
    data: {
      breadcrumb: "Livraisons"
    }
  },
  {
    path: 'paiement',
    loadChildren: () => import('../../components/paiements/paiements.module').then(m => m.PaiementsModule),
    data: {
      breadcrumb: "Paiements"
    }
  },
   
 


  {
    path: 'settings',
    loadChildren: () => import('../../components/setting/setting.module').then(m => m.SettingModule),
    data: {
      breadcrumb: "Settings"
    }
  },
  
];
