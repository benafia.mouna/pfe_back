import { Injectable, HostListener, Inject } from '@angular/core';
import { BehaviorSubject, Observable, Subscriber } from 'rxjs';
import { WINDOW } from "./windows.service";
import jwt_decode from "jwt-decode";
import { AuthService } from 'src/service/authService';
import { UserService } from 'src/service/userService';

// Menu
export interface Menu {
	path?: string;
	title?: string;
	icon?: string;
	type?: string;
	badgeType?: string;
	badgeValue?: string;
	active?: boolean;
	bookmark?: boolean;
	children?: Menu[];
}

@Injectable({
	providedIn: 'root'
})

export class NavService {

	public screenWidth: any
	public role:any=''
	public collapseSidebar: boolean = false

	constructor(@Inject(WINDOW) private window, private authService: AuthService, private userservice: UserService) {
		this.onResize();
		if (this.screenWidth < 991) {
			this.collapseSidebar = true
		}

		this.getRole()

		console.log('roleee ',localStorage.getItem("role"))
	}

	// Windows width
	@HostListener("window:resize", ['$event'])
	onResize(event?) {
		this.screenWidth = window.innerWidth;
	}

	getRole(){
		var decoded = jwt_decode(localStorage.getItem("token"))
 
	console.log("decoded : ",decoded);

	}

	
	MENUITEMS: Menu[] =localStorage.getItem("role")==="ADMIN"? [
		{
			path: '/dashboard/default', title: 'Dashboard', icon: 'home', type: 'link', badgeType: 'primary', active: false
		},
		{
			title: 'Promotions', icon: 'box', type: 'sub', active: false, children: [
				
						
						{ path: '/promotions/promotion-list', title: 'Promotion List', type: 'link' },
						{ path: '/promotions/add-promotion', title: 'Add Promotion', type: 'link' },
						{ path: '/promotions/notification', title: 'All notification', type: 'link' },
					]
		},
		{
			title: 'Sales', icon: 'dollar-sign', type: 'sub', active: false, children: [
				{ path: '/sales/orders', title: 'Orders', type: 'link' },
				
			]
		},

		{
			title: 'Users', icon: 'user-plus', type: 'sub', active: false, children: [
			
				{ path: '/users/list-acheteur', title: 'Buyer List', type: 'link' },
				{ path: '/users/list-vendeur', title: 'Seller List', type: 'link' },
			]
		},
		
		
		
		{
            title: 'Ordre', icon: 'dollar-sign', type: 'sub', active: false, children: [
              { path: '/ordre/orders', title: 'Orders List', type: 'link' },
            
            ]
          },
          {
            title: 'Delivery', icon: 'dollar-sign', type: 'sub', active: false, children: [
              { path: '/livraison/livraison', title: 'Delivery List', type: 'link' },
            ]
          },
          {
            title: 'Payment', icon: 'dollar-sign', type: 'sub', active: false, children: [
              { path: '/paiement/paiements', title: 'Payment List', type: 'link' },
              
            ]
          },
		{
			title: 'Settings', icon: 'settings', type: 'sub', children: [
			
				{ path: '/settings/gerer-permission', title: 'Manage Permission', type: 'link' },
				{ path: '/settings/list-permission', title: 'Permission List', type: 'link' },
				{ path: '/settings/categorie', title: 'Category List', type: 'link' },
				{ path: '/settings/sub-categorie', title: 'Sub Category List', type: 'link' },
				{ path: '/settings/marque', title: 'Mark List', type: 'link' },
				{ path: '/settings/country', title: 'Country List', type: 'link' },
				{ path: '/settings/city', title: 'City List', type: 'link' },
				{ path: '/settings/mode-livraison', title: 'Mode livraison List', type: 'link' },
                { path: '/settings/mode-paiement', title: 'Mode paiement List', type: 'link' },
			]
		},
		{
			title: 'Login',path: '/auth/login', icon: 'log-in', type: 'link', active: false
		}
	] : [{
		path: '/dashboard/default', title: 'Dashboard', icon: 'home', type: 'link', badgeType: 'primary', active: false
	},{
		title: 'Logout',path: '/auth/login', icon: 'log-in', type: 'link', active: false
	}]
	
	
	  
	items = new BehaviorSubject<Menu[]>(this.MENUITEMS);


}
