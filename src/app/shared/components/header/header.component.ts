import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/service/userService';
import { NavService } from '../../service/nav.service';
import Swal from 'sweetalert2'
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/model/user';
import { MailService } from 'src/service/mailService';
import { Mail } from 'src/model/mail';
import { PromoService } from 'src/service/promoService';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public right_sidebar: boolean = false;
  public open: boolean = false;
  public openNav: boolean = false;
  addForm: FormGroup;
  user: User;
  public isOpenMobile: boolean;
  public closeResult: string;
  listpromo: any
  @Output() rightSidebarEvent = new EventEmitter<boolean>();
  userconnect: any
  listuser: any;
  email = localStorage.getItem("email")

  constructor(private promoService: PromoService, private mailService: MailService, private form: FormBuilder, public navServices: NavService, private userservice: UserService, private router: Router, private modalService: NgbModal) { }


  collapseSidebar() {
    this.open = !this.open;
    this.navServices.collapseSidebar = !this.navServices.collapseSidebar
  }
  logout() {

    localStorage.clear()
    this.router.navigate(["/auth/login"])
  }
  right_side_bar() {
    this.right_sidebar = !this.right_sidebar
    this.rightSidebarEvent.emit(this.right_sidebar)
  }

  openMobileNav() {
    this.openNav = !this.openNav;
  }


  ngOnInit() {
    this.addForm = this.form.group({
      subject: ['', Validators.required],
      content: ['', Validators.required],


    });
    this.findUserByEmail()
    this.getallusers()
    this.getallpromos()

  }

  findUserByEmail() {
    this.userservice.findUser(this.email).subscribe(async res => {
      await new Promise(resolve => setTimeout(resolve, 200));
      this.user = res
    })

  }
  isvendeur() {
    return this.user?.roles[0]?.name == "VENDEUR" ? true : false
  }
  isadmin() {
    return this.user?.roles[0]?.name == "ADMIN" ? true : false
  }
  getallpromos() {
    this.promoService.getPromo().subscribe(res => {
      this.listpromo = res

      this.listpromo = this.listpromo.filter(item => item.status == "en cours" && item.view == "0")

    })
  }


  getallusers() {
    this.userservice.getUsers().subscribe(res => {
      this.listuser = res
    })
  }
  openn(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  Envoyermail() {
    const m: Mail = {
      subject: this.addForm.value.subject,
      content: this.addForm.value.content,
      to: localStorage.getItem("email"),
      from: "genova.angel@hotmail.com"
    };
    //console.log('7eta lennna cv ca marche tres bien', m.to, m.subject, m.content)
    this.mailService.sendmail(m).subscribe((res: any) => {
      console.log("response of send mail : ", res)

    });
  }
  updatePromo(item: any, id: any) {

    item.view = 1

    console.log("item : ",item,id)
    this.promoService.updatennotif(item, id).subscribe((res: any) => {
      console.log("res of update ", res)

     // location.reload();
    })


  }

}
