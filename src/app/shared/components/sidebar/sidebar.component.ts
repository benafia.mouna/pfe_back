import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { User } from 'src/model/user';
import { UserService } from 'src/service/userService';
import { NavService, Menu } from '../../service/nav.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SidebarComponent implements OnInit {

  public menuItems: Menu[];
  public url: any;
  public fileurl: any;
  userconnect: User
  listuser: any;
  role: any
  email = localStorage.getItem("email")
  constructor(private router: Router, public navServices: NavService, private userservice: UserService) {

    this.findUserByEmail()


  }

  // Active Nave state
  setNavActive(item) {
    this.menuItems.filter(menuItem => {
      if (menuItem != item)
        menuItem.active = false
      if (menuItem.children && menuItem.children.includes(item))
        menuItem.active = true
      if (menuItem.children) {
        menuItem.children.filter(submenuItems => {
          if (submenuItems.children && submenuItems.children.includes(item)) {
            menuItem.active = true
            submenuItems.active = true
          }
        })
      }
    })
  }

  // Click Toggle menu
  toggletNavActive(item) {
    if (!item.active) {
      this.menuItems.forEach(a => {
        if (this.menuItems.includes(item))
          a.active = false
        if (!a.children) return false
        a.children.forEach(b => {
          if (a.children.includes(item)) {
            b.active = false
          }
        })
      });
    }
    item.active = !item.active
  }

  //Fileupload
  readUrl(event: any) {
    if (event.target.files.length === 0)
      return;
    //Image upload validation
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    // Image upload
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.url = reader.result;
    }
  }
  ngOnInit() {
    this.getallusers()

  }
  getallusers() {

    this.userservice.getUsers().subscribe(res => {
      console.log(res)
      this.listuser = res
      console.log("list user", this.listuser)
    })
  }
  async findUserByEmail() {
    console.log("email", this.email)
    await this.userservice.findUser(this.email).subscribe(res => {
      this.userconnect = res
      this.role = this.userconnect.roles[0].name;
      console.log("role", this.role)
      localStorage.setItem('role', this.role)

      this.menuItems = this.role === "ADMIN" ? [
        {
          path: '/dashboard/default', title: 'Dashboard', icon: 'home', type: 'link', badgeType: 'primary', active: false
        },
        {
          title: 'Promotions', icon: 'box', type: 'sub', active: false, children: [


            { path: '/promotions/promotion-list', title: 'Promotion List', type: 'link' },
            { path: '/promotions/notification', title: 'All notification', type: 'link' },
          ]
        },

        {
          title: 'Users', icon: 'user-plus', type: 'sub', active: false, children: [
            
            { path: '/users/list-acheteur', title: 'Buyer List', type: 'link' },
            { path: '/users/list-vendeur', title: 'Seller List', type: 'link' },


          ]
        },

        {
          title: 'Settings', icon: 'settings', type: 'sub', children: [
            
            { path: '/settings/gerer-permission', title: 'Manage Permission', type: 'link' },
            { path: '/settings/list-permission', title: 'Permission List', type: 'link' },
            { path: '/settings/categorie', title: 'Category List', type: 'link' },
            { path: '/settings/sub-categorie', title: 'Sub Category List', type: 'link' },
            { path: '/settings/marque', title: 'Mark List', type: 'link' },
            { path: '/settings/country', title: 'Country List', type: 'link' },
            { path: '/settings/city', title: 'City List', type: 'link' },
            { path: '/settings/mode-livraison', title: 'Delivery method List', type: 'link' },
            { path: '/settings/mode-paiement', title: 'Payment method List', type: 'link' },
          ]
        },

      ] :

        [
          {
            path: '/dashboard/default', title: 'Dashboard', icon: 'home', type: 'link', badgeType: 'primary', active: false
          },
          {
            title: 'Promotion', icon: 'box', type: 'sub', active: false, children: [


              { path: '/promotions/promotion-list', title: 'Promotion List', type: 'link' },
              { path: '/promotions/add-promotion', title: 'Add Promotion', type: 'link' },
           
            ]
          },
          {
            title: 'Orders', icon: 'dollar-sign', type: 'sub', active: false, children: [
              { path: '/orders/orders', title: 'Orders List', type: 'link' },
            
            ]
          },
          {
            title: 'Delivery', icon: 'home', type: 'sub', active: false, children: [
              { path: '/livraison/livraison', title: 'Delivery List', type: 'link' },
            ]
          },
          {
            title: 'Payment', icon: 'dollar-sign', type: 'sub', active: false, children: [
              { path: '/paiement/paiement', title: 'Payment List', type: 'link' },
              
            ]
          },
        
        ]

    })
  }
  isadmin() {
    return this.userconnect?.roles[0]?.name == "ADMIN" ? true : false
  }

  isvendeur() {
    return this.userconnect?.roles[0]?.name == "VENDEUR" ? true : false
  }

  logout() {

    localStorage.clear()
    this.router.navigate(["/auth/login"]);
  }




}
