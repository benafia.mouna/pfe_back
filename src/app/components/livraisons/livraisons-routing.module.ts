import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LivraisonComponent } from './livraison/livraison.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'livraison',
        component: LivraisonComponent,
        data: {
          title: "Delivery List",
          breadcrumb: "Delivery List"
        }
      },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LivraisonsRoutingModule { }
