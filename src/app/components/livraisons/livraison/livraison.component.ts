import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { LivraisonService } from 'src/service/livraisonService';
import { UserService } from 'src/service/userService';
import { PromoService } from 'src/service/promoService';
import { ModeLivraisonService } from 'src/service/modelivraison';

@Component({
  selector: 'app-livraison',
  templateUrl: './livraison.component.html',
  styleUrls: ['./livraison.component.scss']
})
export class LivraisonComponent implements OnInit {
  public addFormLivraison: FormGroup;
  public addFormLivraison1: FormGroup;
  listlivraison: any = []
  deletelivraison: any
  listmodeliv: any = []
  orders : any
  listcommande: any = []
  ordes: any = []
  search_name: any
  p: number = 1;
  email = localStorage.getItem("email")
  public closeResult: string;
  constructor(private modelivraisonService: ModeLivraisonService, private promoService: PromoService, private userservice: UserService, private modalService: NgbModal, private livraisonService: LivraisonService, private form: FormBuilder) { }


  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open1(content1: any, livraison: any) {
    this.addFormLivraison1.patchValue({
      id:livraison.id,
      id_commande: livraison.commande.id,
      id_modelivraison: livraison.modelivraison.id,
      date: livraison.date,
      status: livraison.status,

    })
    this.modalService.open(content1, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  ngOnInit() {

    this.addFormLivraison = this.form.group({
      date: ['', Validators.required],
      id_modelivraison: ['', Validators.required],
      id_commande: ['', Validators.required],
      status: ['', Validators.required]
    });

    this.addFormLivraison1 = this.form.group({
      id: ['', Validators.required],
      date: ['', Validators.required],
      status: ['', Validators.required],
      id_modelivraison: ['', Validators.required],
      id_commande: ['', Validators.required]
    });

    this.findUserByEmail()
    this.getalllivraison()
   
    this.getallmodelivraison()

  }
  getalllivraison() {

    this.livraisonService.getLivraison().subscribe(res => {
      this.listlivraison = res
      console.log("list livraison", this.listlivraison)
      
      
    })
  }
  onUpdatePaiement() {
    this.livraisonService.updateLivraison(this.addFormLivraison1.value,this.addFormLivraison1.value.id_modepaiement, this.addFormLivraison1.value.id ,this.addFormLivraison1.value.id_commande).subscribe(
      (reponse: any) => {
        console.log('aaaaaaaaaaaaaaaaaaaaaaaatrwewe ',this.addFormLivraison1.value.id_modepaiement,this.addFormLivraison1.value,this.addFormLivraison1.value.id);
        Swal.fire({
          icon: 'success',
          title: 'Updated successfully !',
          showConfirmButton: false,
          timer: 1500
        })
        this.getalllivraison();
      },
    );
  }

  onAddLivraison() {

    console.log("liv : ", this.addFormLivraison.value)
    this.livraisonService.addLivraison(this.addFormLivraison.value,this.addFormLivraison.value.id_modelivraison, this.addFormLivraison.value.id_commande).subscribe(
      (reponse: any) => {
        console.log(reponse);
        this.getalllivraison();
      },
    );
  }


  onUpdateLivraison() {
    this.livraisonService.updateLivraison(this.addFormLivraison1.value,this.addFormLivraison1.value.id_modelivraison, this.addFormLivraison1.value.id_commande, this.addFormLivraison1.value.id ).subscribe(
      (reponse: any) => {
        console.log('tt',reponse);
        Swal.fire({
          icon: 'success',
          title: 'Updated successfully !',
          showConfirmButton: false,
          timer: 1500
        })
        this.getalllivraison();
      },
    );
  }
  onDeleteLivraison(livraisonId:number ):void{ 
    console.log(livraisonId);
    this.livraisonService.deleteLivraison(livraisonId).subscribe(
      (reponse: void) => {
        console.log(reponse);
        this.getalllivraison();
      },
    );
    }

  public onOpenModal(livraisonId: number): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.livraisonService.deleteLivraison(livraisonId).subscribe(
          (reponse: void) => {
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )

            this.getalllivraison();
          },
        );

      }
    })
  }
  findUserByEmail() {
    this.userservice.findUser(this.email).subscribe((res: any) => {
      ///  this.user = res

      this.promoService.getCommandevendeur(res.id).subscribe(res => {
        console.log("all pro",res)
        this.ordes = res
        //this.ordes=this.ordes.filter(item=>!this.Exists(item.id))
        //console.log("all ",this.ordes)
        
      })
    })
  }
  Exists(id) {
   
    return this.ordes.promos.some(function (el) {
     //console.log('waaaaaaaaaaaaaa',(el.id == id.commande.id))
      return el.id == id.commande.promos.id;
    });
  }
  getallmodelivraison() {

    this.modelivraisonService.getModeLivraison().subscribe(res => {
      console.log(res)
      this.listmodeliv = res
      
      console.log("list mode livraison", this.listmodeliv)
    })
  }


}
