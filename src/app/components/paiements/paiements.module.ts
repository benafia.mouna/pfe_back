import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaiementsRoutingModule } from './paiements-routing.module';
import { PaiementComponent } from './paiement/paiement.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [PaiementComponent],
  imports: [
    CommonModule,
    PaiementsRoutingModule,
    Ng2SmartTableModule,
    NgxDatatableModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PaiementsModule { }
