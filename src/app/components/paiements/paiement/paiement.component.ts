import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LivraisonService } from 'src/service/livraisonService';
import { ModeLivraisonService } from 'src/service/modelivraison';
import { ModePaiementService } from 'src/service/modepaiementService';
import { PaiementService } from 'src/service/paiementService';
import { PromoService } from 'src/service/promoService';
import { UserService } from 'src/service/userService';
import Swal from 'sweetalert2';
import { ModePaiementComponent } from '../../setting/mode-paiement/mode-paiement.component';

@Component({
  selector: 'app-paiement',
  templateUrl: './paiement.component.html',
  styleUrls: ['./paiement.component.scss']
})
export class PaiementComponent implements OnInit {
  public addFormPaiement: FormGroup;
  public addFormPaiement1: FormGroup;
  listpaiement: any = []
  deletepaiement: any
  listmodepaie: any = []
  listcommande: any = []
  ordes: any = []
  search_name: any
  p: number = 1;
  email = localStorage.getItem("email")
  public closeResult: string;

  constructor(private modepaiementService: ModePaiementService, private promoService: PromoService, private userservice: UserService, private modalService: NgbModal, private paiementservice: PaiementService, private form: FormBuilder) { }


  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  open1(content1: any, paiement: any) {
   console.log(' name', paiement.modePaiement.name)
       this.addFormPaiement1.patchValue({
        id:paiement.id,
        id_commande:paiement.commande.id,
        id_modepaiement:paiement.modePaiement.id,
        status: paiement.status,
        date:paiement.date,
       
       })
       console.log('id_commande: ',paiement.commande.id)
     this.modalService.open(content1, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
     
      this.closeResult = `Closed with: ${result}`;
     }, (reason) => {

       this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
       console.log('id_commande: ',)
     });
  }
  ngOnInit() {

    this.addFormPaiement = this.form.group({
      date: ['', Validators.required],
      id_modepaiement: ['', Validators.required],
      id_commande: ['', Validators.required],
      status: ['', Validators.required]
    });

    this.addFormPaiement1 = this.form.group({
      id: ['', Validators.required],
      date: ['', Validators.required],
      status: ['', Validators.required],
      id_modepaiement: ['', Validators.required],
      id_commande: ['', Validators.required]
    });

    this.getallpaiement()
    this.findUserByEmail()
    this.getallmodepaiement()

  }
  getallpaiement() {

    this.paiementservice.getPaiement().subscribe(res => {
      this.listpaiement = res
      console.log("list paiement", this.listpaiement)
    })
  }


 
  onAddPaiement() {

    console.log("liv : ", this.addFormPaiement.value)
    this.paiementservice.addPaiement(this.addFormPaiement.value,this.addFormPaiement.value.id_commande,this.addFormPaiement.value.id_modepaiement ).subscribe(
      (reponse: any) => {
        console.log(reponse);
        this.getallpaiement();
      },
    );
  }


  onUpdatePaiement() {
    this.paiementservice.updatePaiement(this.addFormPaiement1.value,this.addFormPaiement1.value.id_modepaiement, this.addFormPaiement1.value.id ,this.addFormPaiement1.value.id_commande).subscribe(
      (reponse: any) => {
        console.log('aaaaaaaaaaaaaaaaaaaaaaaatrwewe ',this.addFormPaiement1.value.id_modepaiement,this.addFormPaiement1.value,this.addFormPaiement1.value.id);
        Swal.fire({
          icon: 'success',
          title: 'Updated successfully !',
          showConfirmButton: false,
          timer: 1500
        })
        this.getallpaiement();
      },
    );
  }

  onDeletePaiement(paiementId: number): void {
    this.paiementservice.deletePaiement(paiementId).subscribe(
      (reponse: void) => {
        console.log(reponse);
        this.getallpaiement();
      },
    );
  }

  public onOpenModal(paiementId: any): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.paiementservice.deletePaiement(paiementId).subscribe(
          (reponse: void) => {
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )

            this.getallpaiement();
          },
        );

      }
    })
  }
  findUserByEmail() {
    this.userservice.findUser(this.email).subscribe((res: any) => {
      ///  this.user = res

      this.promoService.getCommandevendeur(res.id).subscribe(res => {
        console.log("all pro",res)
        this.ordes = res
        this.ordes=this.ordes.filter(item=>!this.Exists(item.id))
        console.log("all listpaiement",this.ordes)
        
      })
    })
  }
  Exists(id) {
   
    return this.listpaiement.some(function (el) {
     
      return el.commande.id == id;
    });
  }
  getallmodepaiement() {
    this.modepaiementService.getModePaiement().subscribe(res => {
      console.log(res)
      this.listmodepaie = res
      console.log("list mode paiement", this.listmodepaie)
    })
  }


}
