import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaiementComponent } from './paiement/paiement.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'paiement',
        component: PaiementComponent,
        data: {
          title: "Payment List",
          breadcrumb: "Payment List"
        }
      },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaiementsRoutingModule { }
