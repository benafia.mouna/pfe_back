import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Validation from 'src/helper/match';
import { AuthService } from 'src/service/authService';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {
  public resetForm: FormGroup;
  submitted = false;
  resetLink=this.activeroute.snapshot.params['resetLink']
  constructor(private formBuilder: FormBuilder, private authService: AuthService,private activeroute: ActivatedRoute) {
    this.changepassword() 
    localStorage.clear();
   }
   changepassword() {
    this.resetForm = this.formBuilder.group({
      newPassword: ['', Validators.required],
       confirmPassword: ['', Validators.required]
    },
       {
         validators: Validation.match('newPassword', 'confirmPassword')
      }
     );
  }
  change(){
    this.submitted = true;
     if (this.resetForm.invalid) {
       return;
     }
    console.log('resetLink',this.resetLink)
    console.log('newPassword',this.resetForm.value.newPassword)

    this.authService.savePassword(this.resetLink,this.resetForm.value.newPassword).subscribe(
      data=>{
        if(data == "success"){
          Swal.fire(
            'Good job!',
            'Reset password success!',
            'success'
          )
        }
      }
    )
  }
  get f() { return this.resetForm.controls; }

  ngOnInit(): void {
  }

}
