import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DesignutilityServiceService } from 'src/app/designutility-service.service';
import Validation from 'src/helper/match';
import { User } from 'src/model/user';
import { AuthService } from 'src/service/authService';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  showPassword: boolean = false;
  user = new User();
  userid: any;
  public loginForm: FormGroup;
  public registerForm: FormGroup;
  editdata: any;
  responsedata: any;
  errorMessage = '';
  messagelogin = '';
  test = '';
  fileToUpload: Array<File> = [];
  submitted = false;
  submitted1 = false;
  user1:any
  result : any
  unautretest = false;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {
    this.createLoginForm();
    this.createRegisterForm();
    localStorage.clear();
  }

  owlcarousel = [
    {
      title: 'Promotion.tn',
      desc: '100% PROMO Take advantage of our incredible offers at UNBEATABLE PRICES !!!',
    },
  ];

  owlcarouselOptions = {
    loop: true,
    items: 1,
    dots: true
  };

  createLoginForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

 

  createRegisterForm() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      // acceptTerms: [false, Validators.requiredTrue],
      email: ['',[
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      phone: ['',[Validators.required]],
      type: ['',[Validators.required]],
  

   
    }, {
      validators: Validation.match('password', 'confirmPassword')

    });
  }
  handleFileInput(files: any) {
    this.fileToUpload = <Array<File>>files.target.files;
    console.log("file To Upload " + this.fileToUpload);
  }

  ngOnInit(): void {
   

  }
  get f() { return this.registerForm.controls; }

  register() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }

    let formdata= new FormData()
    formdata.append("firstName",this.registerForm.value.firstName)
    formdata.append("password",this.registerForm.value.password)
    formdata.append("lastName",this.registerForm.value.lastName)
    formdata.append("email",this.registerForm.value.email)
    formdata.append("phone",this.registerForm.value.phone)
    formdata.append("type",this.registerForm.value.type)
    formdata.append("file",this.fileToUpload[0]);
    formdata.append("roles","VENDEUR");


   
    
    this.authService.register(formdata).subscribe((res:any)=>{
      if (res == ''){
        Swal.fire({
      
          title: 'User already exists !',
      
        })
      }else{
        
        this.router.navigateByUrl("/confirm")
      }
      
    });
    
 

  }
  showHidePassword() {
    this.showPassword = !this.showPassword;
  }
   login() {

    this.user.email = this.loginForm.value.username;
    this.user.password = this.loginForm.value.password;
   

    if (this.loginForm.valid) {
      this.authService.VerificationMDP(this.user).subscribe(voila => {
        console.log('reponse de verification de mot de passe : ', voila)
        if (voila === true) {
          this.authService.findUserByEmail(this.loginForm.value.username).subscribe((res:any) => {
           
            if (res === true) {
              this.authService.login(this.loginForm.value).subscribe(
                result => {
                     if (result != null) {
                    console.log(this.loginForm.value.username);
                    this.responsedata = result;
                    console.log("result : ",result);
                    localStorage.setItem('token',  result);
                    localStorage.setItem("email",this.loginForm.value.username)
                    
                    localStorage.setItem("state","1")
                    
                    this.authService.findUser(this.loginForm.value.username).subscribe(res=>{
                     
                      localStorage.setItem("role", res.roles[0].name)
                      
                      this.router.navigate(['dashboard/default']);
                    })
                   
                  }
                },
              );
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'your account is not verified!',
                
              })
               this.router.navigate(['/auth/login']);
            }

          }
          );
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Invalid email or password !',
          
          })
           this.router.navigate(['/auth/login']);
        }
      })

    }
    
   
  }

  //  sleep(ms) {
  //   return new Promise((resolve) => {
  //     setTimeout(resolve, ms);
  //   });
  // }
  isvendeur(){
    return this.user?.roles[0]?.name=="VENDEUR"? true:false
  }
  

}
