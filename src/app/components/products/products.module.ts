import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CKEditorModule } from 'ngx-ckeditor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductsRoutingModule } from './products-routing.module';
import { SubCategoryComponent } from '../setting/sub-category/sub-category.component';
import { ProductListComponent } from './product-list/product-list.component';
import { AddProductComponent } from './add-product/add-product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { GalleryModule } from '@ks89/angular-modal-gallery';
import 'hammerjs';
import 'mousetrap';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { ToastrModule } from 'ngx-toastr';
import { MarqueComponent } from '../setting/marque/marque.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FullCalendarModule } from '@fullcalendar/angular';
import { CategoryComponent } from '../setting/category/category.component';
import { RecherchePipe } from 'src/app/pipes/recherche.pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { UpdatePromoComponent } from './update-promo/update-promo.component';
import { NotificationComponent } from './notification/notification.component';

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  maxFilesize: 50,
  url: 'https://httpbin.org/post',
};


@NgModule({
  declarations: [ ProductListComponent, AddProductComponent, ProductDetailComponent ,   RecherchePipe , UpdatePromoComponent,    NotificationComponent ],
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    CKEditorModule,
    ProductsRoutingModule,
    Ng2SmartTableModule,
    NgbModule,
    DropzoneModule,
    FullCalendarModule,
    Ng2SearchPipeModule,
    GalleryModule.forRoot()
  ],
 
})
export class ProductsModule { }
