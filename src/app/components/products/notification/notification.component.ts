import { Component, OnInit } from '@angular/core';
import { PromoService } from 'src/service/promoService';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  listpromo:any = []
  search_name:any
  deletNotification:any
  p: number = 1;
  promoId:any
  constructor(private promoService:PromoService) { }

  ngOnInit(): void {
    this.getallpromos() 
  }
  getallpromos() {
    this.promoService.getPromo().subscribe(res => {

      console.log("res of notif ",res)
      this.listpromo = res

      this.listpromo = this.listpromo.filter(item => item.status == "en cours")

    })
  }
  public onOpenModal(promoId: number): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        // this.promoService.del(promoId).subscribe(
        //   (reponse: void) => {
        //     Swal.fire(
        //       'Deleted!',
        //       'Your file has been deleted.',
        //       'success'
        //     )
        //     console.log(reponse);
        //     this.getallpromos();
        //   },
        // );

      }
    })
  }

  onDeletNotification(categorieId: number): void {
    console.log(categorieId);

  }
  updatePromo(item: any, id: any) {

   // item.view = 1

    console.log("item : ",item)
    this.promoService.noupdatennotif(item, id).subscribe((res: any) => {
      console.log("res of update ", res)
     this.getallpromos() 

     // location.reload();
    })


  }
}
