import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { productDB } from 'src/app/shared/tables/product-list';
import { User } from 'src/model/user';
import { CategorieService } from 'src/service/categorieService';
import { ItemService } from 'src/service/ItemService';
import { PromoService } from 'src/service/promoService';
import { UserService } from 'src/service/userService';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  public product_list = []
  promot : any
  x:any
  y:any
  addFormItem: FormGroup;
  discount:any
  RecherchePromoCa : any
  listpromo : any
  deletPromo : any
  search_name : any
  listitem : any
  ids:any=[];
  listcategorie : any=[];
  p: number = 1;
  user: User;
  email= localStorage.getItem("email")

  constructor(private categorieService : CategorieService ,private itemService : ItemService,private promoService : PromoService,private userservice : UserService) {

  }

  ngOnInit() {
    this.findUserByEmail()
    this.getallcategorie()
    this.calcul()
    
   
  }
  calcul(){
    this.itemService.findDiscount("1").subscribe(
      (res=>{
        this.discount=res
        console.log("discouuuuunt",this.discount)
      })
     
    )
  }
  async filtrerListPromo(name:any){
    let x = 0
    let Alist = []
    this.listpromo = []
    let test = false
    this.getPromoForRech()
    
    await new Promise(resolve => setTimeout(resolve, 100));
    
   
    for(let i = 0;i<this.RecherchePromoCa.length;i++){
       if(this.RecherchePromoCa[i].subcategorie?.categorie?.name == name.target.value){
       test = true

        Alist[x] = this.RecherchePromoCa[i]
       x = x +1
        }
  }
  if (test == false){
    Alist = []

  }
  if(name.target.value == "Choose category"){
    this.listpromo = this.RecherchePromoCa
  }else{
    this.listpromo = Alist
  }
  
  Alist = []
  x=0
  test = false
  this.RecherchePromoCa = []
  
}
calculprixpromo() {
  this.x =  (this.addFormItem.value.prixreel - ((this.addFormItem.value.prixreel/100) * this.addFormItem.value.remise)) 
  this.addFormItem.value.prixpromo = this.x
  this.y = this.x
  this.addFormItem.patchValue({
  prixpromo:  this.y
  })


}

getcategorie(q:any){
  localStorage.setItem("idpromo",q?.id)
  localStorage.setItem("categName",q?.subcategorie.name)

}
  getallpromos(id:any) {
    console.log("id of get All Promo ",id)
    this.promoService.getPromoBy(id).subscribe(res => {
      
      this.listpromo = res
      console.log("listpromo",this.listpromo)
      
    })
  }
  getallpromoss():any {
    this.promoService.getPromo().subscribe(res => {
      this.listpromo = res
      
      
      console.log("listpromo",this.listpromo)
      return res
    })
   
  }
  formatNumber = function(i) {
    return Math.round(i); 
}
  getPromoForRech(){
    if(this.user.roles[0].name=="ADMIN"){

this.promoService.getPromo().subscribe(res => {
  this.RecherchePromoCa = res
  
  
  
})
    }else{

      this.promoService.getPromoBy(this.user.id).subscribe(res => {
      
        this.RecherchePromoCa = res
        
        
      })
    }
  }
  findUserByEmail() {
    this.userservice.findUser(this.email).subscribe((res:any) => {
      this.user=res
      console.log("role of user",this.user.roles[0].name)
      if(this.user.roles[0].name=="ADMIN")
       this.getallpromoss()
      else
      this.getallpromos(this.user.id)

    })
  }
  isvendeur(){
    return this.user?.roles[0]?.name=="VENDEUR"? true:false
  }
  isadmin(){
    return this.user?.roles[0]?.name=="ADMIN"? true:false
  }
  onDeletPromo(promoId: number): void {
    console.log(promoId);

  }

  public onOpenModal(id:number): void { 

    this.itemService.getItem().subscribe((res:any) => {
      console.log(res)
    
      this.listitem = res
     .filter((el:any)=>el.promo.id==id)
      console.log( "list item",this.listitem)
      for(let i=0;i < this.listitem.length;i++){
        this.ids.push(this.listitem[i].id)
      }
      console.log( "list item",this.listitem)
    })

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.promoService.deletePromo(id,this.ids).subscribe(
          (reponse) => {
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            location.reload();
            window.location.href="http://localhost:4300/promotions/promotion-list"
            console.log(reponse);
            this.getallpromos(this.user.id);
          },
        );
        
      }
    })
  }
  getallcategorie() {

    this.categorieService.getCategorie().subscribe(res => {
      console.log(res)
      this.listcategorie = res
      console.log("list categorie", this.listcategorie)
    })
  }
 
}
