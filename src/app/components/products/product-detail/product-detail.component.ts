import { Component, Input, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Image } from '@ks89/angular-modal-gallery';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { ItemService } from 'src/service/ItemService';
import { PromoService } from 'src/service/promoService';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/service/userService';
import { User } from 'src/model/user';
import Swal from 'sweetalert2';
import { diffDates } from '@fullcalendar/angular';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
  providers: [NgbRatingConfig]
})
export class ProductDetailComponent implements OnInit {

  currentDate: any;
  p: number = 1;
  targetDate: any;
  cDateMillisecs: any;
  tDateMillisecs: any;
  difference: any;
  seconds: any;
  minutes: any;
  hours: any;
  days: any;
  year: number = 2023;
  month: number = 6;
  months = [
    'Jan',
    'Feb',
    'Mar',
    'April',
    'May',
    'June',
    'July',
    'Aug',
    'Sept',
    'Oct',
    'Nov',
    'Dec',
  ];
  day: number = 31;
  public closeResult: string;
  result: any
  user: User;
  public counter: number = 1;
  id = this.activeroute.snapshot.params['idpromo']
  listitem: any
  k: any = 0;
  email = localStorage.getItem("email")
  image: any = [

  ]

  indexs = []
  promo: any
  y: any
  m: any
  d: any


  h:any;
  mm:any
  ss:any
  z = 0;

  public imagesRect: Image[] = [

    new Image(0, { img: 'http://127.0.0.1:8887/img' }, { img: 'http://127.0.0.1:8887/img' }),
    new Image(1, { img: 'http://127.0.0.1:8887/img' }, { img: 'http://127.0.0.1:8887/img' }),
    new Image(2, { img: 'assets/images/pro3/1.jpg' }, { img: 'assets/images/pro3/1.jpg' }),
    new Image(3, { img: 'assets/images/pro3/2.jpg' }, { img: 'assets/images/pro3/2.jpg' }),



  ]

  constructor(private userService: UserService, private modalService: NgbModal, config: NgbRatingConfig, private itemService: ItemService, private promoService: PromoService, private activeroute: ActivatedRoute, private route: Router) {
    config.max = 5;
    config.readonly = false;


  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  increment() {
    this.counter += 1;
  }

  decrement() {
    this.counter -= 1;
  }
  async getallitems() {

    await this.itemService.getItem().subscribe((res: any) => {
      
      this.imagesRect.length = 0
      this.listitem = res.filter((el: any) => el.promo.id == this.id)


      this.listitem.forEach((item, i) => {

        console.log("ok i ", i)
        var result = [];
        item.photos.forEach(function (item) {
          if (result.indexOf(item) < 0) {
            result.push(item);
          }
        });
        if (i > 0) {
          const toRemove = new Set(this.listitem[i - 1].photos);
          //   console.log("toRemove :", toRemove)
          ///  console.log("result :", result)

          const difference = result.filter(x => !toRemove.has(x));

          /// console.log("difference ", difference)
          this.indexs[i] = {
            k: i, items: difference.map(item =>

              new Image(i, { img: 'http://127.0.0.1:8887/' + item }, { img: 'http://127.0.0.1:8887/' + item }),

            )
          }
        } else {

          this.indexs[i] = {
            k: i, items: result.map(item =>

              new Image(i, { img: 'http://127.0.0.1:8887/' + item }, { img: 'http://127.0.0.1:8887/' + item }),

            )
          }
        }

      })
    })
  }
  isadmin() {
    return this.user?.roles[0]?.name == "ADMIN" ? true : false
  }

  findUserByEmail() {
    this.userService.findUser(this.email).subscribe(res => {
      this.user = res


    })
  }

  ngOnInit() {

    this.getallitems()
    this.getOnepromo()
    this.findUserByEmail()




  }
  getOnepromo() {
    this.promoService.getOnePromo(this.id).subscribe(res => {
      this.promo = res


     console.log("promoooooooooo", new Date(this.promo.date_fin).getMinutes())
      this.y = new Date(this.promo.date_fin).getFullYear();
      this.m = new Date(this.promo.date_fin).getMonth()+1
      this.d = new Date(this.promo.date_fin).getDate()
      console.log("date/Month/Day :", this.y, this.m, this.d)
      this.myTimer(this.y, this.m, this.d,new Date(this.promo.date_fin).getMinutes(),new Date(this.promo.date_fin).getSeconds(),new Date(this.promo.date_fin).getHours());

    })
  }

  updatestatus(promo: any) {
    this.promoService.updatestatusconfirm(promo.id).subscribe((res: any) => {
      console.log("status", res)
      Swal.fire({
        icon: 'success',
        title: 'Promo confirmed !',
        showConfirmButton: false,
        timer: 2000
      })
      this.route.navigateByUrl('/promotions/promotion-list');
    })
  }
  ngAfterViewInit() {

  }
  myTimer(y: any, m: any, d: any,mm:any,ss:any,hh:any) {


    if (y != undefined && m != undefined && d != undefined) {


      // 👇️ in Local time
      const date = new Date();
      const year = date.getFullYear();
      const month = date.getMonth() + 1;
      const day = date.getDate();
      const hours = date.getHours();
      const mn = date.getMinutes();
      const mlss = date.getSeconds();

      const startDate = [year, month, day].join('-');
      const timeDate = [hours, mn, mlss].join(':');


      const etime = [hh, mm, ss].join(':');

     

      const eDate = [y, m, d].join('-');


      const start = new Date(startDate + " " + timeDate);

      const endDate = new Date( eDate + ' ' + etime);

      console.log("start: ", start)

      console.log("endDate : ", endDate)

      const diffTime = endDate.getTime() - start.getTime();


      const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

      console.log(diffDays + " days");
      var Seconds_from_T1_to_T2 = diffTime / 1000;
      //var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);

      console.log(Seconds_Between_Dates + " secods");

      const h = Math.round(Math.abs((diffTime-(diffDays*24*60*60*1000))/(60*60*1000)));

      console.log(h + " hours");

      var minutes = Math.round(Math.abs((diffTime-(diffDays*24*60*60*1000)+(h*60*60*1000))/(60*1000)));
      var Seconds_Between_Dates = Math.round(Math.abs(((diffTime-(diffDays*24*60*60*1000)+(h*60*60*1000))+(minutes*60*1000))/(1000)));



      


      // var Difference_In_Time = endDate.getTime() - today.getTime();
      //var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);

      // const days = parseInt((endDate - today) / (1000 * 60 * 60 * 24));
      // const hours = parseInt(Math.abs(endDate - today) / (1000 * 60 * 60) % 24);
      // const minutes = parseInt(Math.abs(endDate.getTime() - today.getTime()) / (1000 * 60) % 60);
      // const seconds = parseInt(Math.abs(endDate.getTime() - today.getTime()) / (1000) % 60);
      // var hours = Math.abs(endDate.getTime() - today.getTime()) / 36e5;
      // var minutes = Math.floor(( Math.abs(endDate.getTime() - today.getTime())/1000)/60);

         document.getElementById('days').innerText = "" + diffDays
      document.getElementById('hours').innerText = '' + h;
       document.getElementById('mins').innerText = "" + minutes;
       document.getElementById('seconds').innerText = "" + Seconds_Between_Dates;

    }

     setInterval(this.getOnepromo,1000);
  }


}




