import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarqueComponent } from 'src/app/components/setting/marque/marque.component';
import { AddProductComponent } from './add-product/add-product.component';
import { CategoryComponent } from '../setting/category/category.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductListComponent } from './product-list/product-list.component';
import { SubCategoryComponent } from '../setting/sub-category/sub-category.component';
import { UpdatePromoComponent } from './update-promo/update-promo.component';
import { NotificationComponent } from './notification/notification.component';



const routes: Routes = [
  {
    path: '',
    children: [
      
      {
        path: 'promotion-list',
        component: ProductListComponent,
        data: {
          title: "Promotion List",
          breadcrumb: "Promotion List"
        }
      },
      {
        path: 'promotion-detail/:idpromo', 
        component: ProductDetailComponent,
        data: {
          title: "Promotion Detail",
          breadcrumb: "Promotion Detail"
        }
      },
      {
        path: 'add-promotion',
        component: AddProductComponent,
        data: {
          title: "Add Promotion",
          breadcrumb: "Add Promotion"
        }
      },
      {
        path: 'update-promo/:id',
        component: UpdatePromoComponent,
        data: {
          title: "update Promotion",
          breadcrumb: "Update Promotion"
        }
      },
      {
        path: 'notification',
        component: NotificationComponent,
        data: {
          title: "All notification",
          breadcrumb: "Notification"
        }
      },
    ]
  },
      
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
