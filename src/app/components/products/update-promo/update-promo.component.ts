import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ItemService } from 'src/service/ItemService';
import Swal from 'sweetalert2';
import { PromoService } from 'src/service/promoService';
import { User } from 'src/model/user';
import { UserService } from 'src/service/userService';
import { MarqueService } from 'src/service/marqueService';
import { ActivatedRoute, Router } from '@angular/router';
import { CategorieService } from 'src/service/categorieService';
import { SubcategorieService } from 'src/service/subcategorieService';

@Component({
  selector: 'app-update-promo',
  templateUrl: './update-promo.component.html',
  styleUrls: ['./update-promo.component.scss']
})
export class UpdatePromoComponent implements OnInit {

  p: number = 1;
  x: number = 0;
  y: number = 0;
  ok = false
  totalprixpromo: number = 0;
  listuser: any;
  listsubcategorie: any;
  idpromo: any
  listcategorie: any;
  user: User;
  testPrix:any;
  remise:any;
  TotalRemise = 0;
  TotalQteRemise = 0;
  public promoForm1: FormGroup
  email = localStorage.getItem("email")
  fileToUpload: Array<File> = [];
  public closeResult: string;
  public promoForm: FormGroup;
  listitem: any = []
  result: any;
  affichTest : Boolean ;
  discount : any;
  categorieName:any;
  id=this.activeroute.snapshot.params['id']
  myFiles: string[] = [];
  listpromo: any;
  promo: any
  addFormItem1: FormGroup;
  public counter: number = 1;
  addFormItem: FormGroup;
  listmarque: any;
  deletItem: any;
  add: boolean = false
  idpromo1:any
  public url = [{
    img: "assets/images/user.png",
  },
  {
    img: "assets/images/user.png",
  },
  {
    img: "assets/images/user.png",
  },
  {
    img: "assets/images/user.png",
  },
  {
    img: "assets/images/user.png",
  },
  {
    img: "assets/images/user.png",
  },
  {
    img: "assets/images/user.png",
  }
  ]
  ngOnInit() {
    console.log(this.ok)
    this.addFormItem = this.form.group({
      id_marque: ['', Validators.required],
      photos: ['', Validators.required],
      name: ['', Validators.required],
      qte: ['', Validators.required],
      prixreel: ['', Validators.required],
      remise: ['', Validators.required],
      detail: [''],
      prixpromo: ['', Validators.required],

    });

    this.addFormItem1 = this.form.group({
      id_marque: ['', Validators.required],
      photos: ['', Validators.required],
      name: ['', Validators.required],
      qte: ['', Validators.required],
      detail: [''],
      prixpromo: ['', Validators.required],
      prixreel: ['', Validators.required],
      remise: ['', Validators.required],

      id: ['', Validators.required]
    });
    this.affichTest= true;
    this.getallitems()
    this.getallpromos()
    this.findUserByEmail()
    this.getallusers()
    this.getallcategorie()
    this.getAllMarques()
    this.getPrixpromo()
    this.findPromo()
    this.calcul()
    

  }
  calculprixpromo1() {
    this.x =  (this.addFormItem1.value.prixreel - ((this.addFormItem1.value.prixreel/100) * this.addFormItem1.value.remise)) 
    this.addFormItem1.value.prixpromo = this.x
    this.y = this.x
    this.addFormItem1.patchValue({
    prixpromo:  this.y
    })

  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  handleFileInput(files: any) {
    this.fileToUpload = <Array<File>>files.target.files;
    console.log("file To Upload " + this.fileToUpload);
  }
  open1(content1, item: any) {
    this.addFormItem1.patchValue({
      id: item.id,
      id_marque: item.marque.id,
      photos: item.photos,
      name: item.name,
      qte: item.qte,
      detail: item.detail,
      prixreel: item.prixreel,
      remise: item.remise,
      prixpromo:item.prixpromo
      
    })
    this.modalService.open(content1, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  onUpdateItem() {

    this.idpromo = localStorage.getItem("idpromo")
    this.addFormItem1.controls['prixreel'].enable()
    this.addFormItem1.controls['name'].enable()
    this.addFormItem1.controls['remise'].enable()
    
    
    let formdata = new FormData()
    formdata.append("name", this.addFormItem1.value.name)
    formdata.append("qte", this.addFormItem1.value.qte)
    formdata.append("detail", this.addFormItem1.value.detail)
    formdata.append("prixreel", this.addFormItem1.value.prixreel)
    formdata.append("remise", this.addFormItem1.value.remise)
    formdata.append("prixpromo", this.addFormItem1.value.prixpromo)
    if(Number(this.addFormItem1.value.remise) < Number(this.discount)){
      Swal.fire({
        
        title: 'Your discount must be at least '+ this.discount+'% of the Real Price',
        showConfirmButton: false,
        timer: 6000
      })
    }else{
    if (this.myFiles.length > 0) {
      for (var i = 0; i < this.myFiles.length; i++) {
        formdata.append("file", this.myFiles[i]);
      }
      this.itemService.updateItem(formdata, this.addFormItem1.value.id_marque, this.idpromo, this.addFormItem1.value.id).subscribe(
        (reponse: any) => {
          console.log(reponse);
          Swal.fire({
            icon: 'success',
            title: 'Updated successfully !',
            showConfirmButton: false,
            timer: 2000
          })
          this.getallitems();
        },
      );
    } else {

      formdata.append("photos", this.addFormItem1.value.photos)

      this.itemService.noupdateItem(formdata, this.addFormItem1.value.id_marque, this.idpromo, this.addFormItem1.value.id).subscribe((res: any) => {
        console.log(res)
        // localStorage.setItem("email",JSON.stringify(res.email))
        // window.location.href="http://localhost:4200/settings/profile"
       
      })
      Swal.fire({
        icon: 'success',
        title: 'Updated successfully !',
        showConfirmButton: false,
        timer: 2000
      })
    
    }
  }
  }
  getallitems() {
    this.idpromo = localStorage.getItem("idpromo")
    this.itemService.getItem().subscribe((res: any) => {
      console.log(res)
      console.log(this.idpromo)
      this.listitem = res
      .filter((el: any) => el.promo.id == this.id)
      console.log("list item", this.listitem)
    })
  }
  getallsubcategorie(e: any) {
    console.log(e.target.value)
    this.subCategorieService.getSubcategorie().subscribe(res => {
      this.listsubcategorie = res.filter((el: any) => el.categorie.id == e.target.value)
    })
  }
  calcul(){
    this.itemService.findDiscount("1").subscribe(
      (res=>{
        this.discount=res
        console.log("discouuuuunt",this.discount)
      })
     
    )
  }
  calculprixpromo() {
    this.x =  (this.addFormItem.value.prixreel - ((this.addFormItem.value.prixreel/100) * this.addFormItem.value.remise)) 
    this.addFormItem.value.prixpromo = this.x
    this.y = this.x
    this.addFormItem.patchValue({
    prixpromo:  this.y
    })


  }
  getallpromos() {
    this.promoService.getPromo().subscribe(res => {
      this.listpromo = res
    })
  }
  onFileChange(event: any) {
    console.log(event);

    for (var i = 0; i < event.target.files.length; i++) {
      this.myFiles.push(event.target.files[i]);
      console.log("ff", this.myFiles)
    }
  }
  getPrixpromo() {
    let totalprix = 0;
    this.listitem.forEach((element: any) => {
      totalprix += Number(element.prixpromo*element.qte);

    });
    this.totalprixpromo = totalprix;

    return Math.round(this.totalprixpromo)
  }

  findPromo() {
    this.promoService.getOnePromo(this.id).subscribe(res => {
      this.promo = res
      console.log("promo", this.promo)
      this.promoForm1.patchValue({
        id:this.promo?.id,
        name: this.promo?.name,
        categorie: this.promo?.subcategorie?.categorie?.name,
        idsubcategorie: this.promo?.subcategorie?.name,
        price: this.promo?.price,
        date_deb: this.promo?.date_deb,
        date_fin: this.promo?.date_fin,
        description: this.promo?.description,
        img: this.promo?.img

      })
   
    })
  }
  getAVG_price(){
    this.categorieName = localStorage.getItem("categName")
  this.itemService.AVG_PRICE(this.addFormItem.value.name,this.categorieName).subscribe(
      (reponse: any) => {
        this.testPrix= Number(reponse)
        
        console.log('ici le prix AVG : ', reponse );
        let prix_User = Number(this.addFormItem.value.prixreel)
        if(this.testPrix != 0){
          if(prix_User > this.testPrix-this.testPrix*(this.discount/100)){
            this.affichTest = true;
            Swal.fire({
              title: 'Your discount must be at least '+ this.discount+'% of the Real Price'
            })
          }else{
            this.remise = prix_User - this.testPrix
            this.remise = Math.round((Number((this.remise/(this.testPrix/100)).toFixed(2))));
            console.log('le prix reel est : ', this.testPrix ,'pourcentage de remise : ', this.remise)
            this.addFormItem.controls['prixreel'].disable();
            this.addFormItem.controls['name'].disable();
            this.affichTest = false;
          }

        }else{
          this.affichTest = false;
          this.remise=0
        }
        
        
      },)
     
  }
  updatePromo() {

    let formdata1 = new FormData()
    formdata1.append("name", this.promoForm1.value.name)
    formdata1.append("date_deb", this.promoForm1.value.date_deb)
    formdata1.append("date_fin", this.promoForm1.value.date_fin)
    formdata1.append("price", this.promoForm1.value.price)
    formdata1.append("description", this.promoForm1.value.description)

    if(this.fileToUpload.length>0) {
    formdata1.append("file", this.fileToUpload[0]);
    this.promoService.updatePromowithFile(formdata1 ,this.promoForm1.value.id).subscribe((res:any)=>{
      console.log(res)
      Swal.fire({
        icon: 'success',
        title: 'Updated successfully !',
        showConfirmButton: false,
        timer: 2000
      })
    })
  }  else {

      formdata1.append("img", this.promoForm1.value.img)

    this.promoService.updatenofile(formdata1, this.promoForm1.value.id).subscribe((res:any)=>{
      console.log(res)

      Swal.fire({
        icon: 'success',
        title: 'Updated successfully !',
        showConfirmButton: false,
        timer: 2000
      })
    })
    }
    
  }
  onAddItem() {
    let x = Math.round(Number(this.testPrix-(-this.testPrix*(this.remise/100))))
    let formdata1 = new FormData()
    this.addFormItem.controls['prixreel'].enable()
    this.addFormItem.controls['name'].enable()
    this.addFormItem.controls['prixpromo'].enable()
    formdata1.append("name", this.addFormItem.value.name)
    formdata1.append("qte", this.addFormItem.value.qte)
    formdata1.append("detail", this.addFormItem.value.detail)
    formdata1.append("prixreel", this.testPrix)
    formdata1.append("remise", this.remise)
    formdata1.append("prixpromo", this.addFormItem.value.prixreel)
    this.addFormItem.controls['prixpromo'].disable()
    for (var i = 0; i < this.myFiles.length; i++) { 
      formdata1.append("file", this.myFiles[i]);
    }
   
   
    this.itemService.addItem(formdata1, this.addFormItem.value.id_marque, this.idpromo).subscribe(
      (reponse: any) => {
        console.log(reponse);
        this.getallitems();
        this.addFormItem.controls['prixreel'].enable()
        this.affichTest = true
       
        this.TotalQteRemise = this.TotalQteRemise + Number(formdata1.get("qte"))
        this.TotalRemise += this.remise*Number(formdata1.get("qte"))
        console.log('remise ici ', this.TotalQteRemise , this.TotalRemise, formdata1.get("qte"))
        this.addFormItem.reset();
      },
    );
    // window.location.href="http://localhost:4200/promotions/add-promotion"
  }
  constructor(private activeroute:ActivatedRoute, private fb: FormBuilder, private categorieService: CategorieService, private modalService: NgbModal, private itemService: ItemService, private form: FormBuilder, private promoService: PromoService, private userService: UserService, private subCategorieService: SubcategorieService, private MarqueService: MarqueService, private route: Router) {


    this.promoForm1 = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      price: ['', Validators.required],
      id: ['', Validators.required],
      date_deb: ['', [Validators.required]],
      date_fin: ['', [Validators.required]],
      description: ['', [Validators.required]],
      idsubcategorie: ['', Validators.required],
      img: ['']
    })
  }



  addp() {
    this.add = true
  }
  getallusers() {

    this.userService.getUsers().subscribe(res => {
      console.log(res)
      this.listuser = res
    })
  }

  findUserByEmail() {
    this.userService.findUser(this.email).subscribe(res => {
      this.user = res

    })
  }

  save() {
    // this.promoForm.patchValue({
    //   price: this.getPrixpromo()
    // })
 
    this.promoService.updatePrix(this.idpromo, this.totalprixpromo, Math.round((this.TotalRemise)*-1/this.TotalQteRemise)).subscribe((res: any) => {
      this.TotalQteRemise = 0
      this.TotalRemise = 0
      console.log(res)
    })
    Swal.fire({
      icon: 'success',
      title: 'Add has been saved',
      showConfirmButton: false,
      timer: 2000
    })
    localStorage.removeItem("idpromo")
    this.route.navigateByUrl("/promotions/promotion-list")

  }
  getallcategorie() {
    this.categorieService.getCategorie().subscribe(res => {
      this.listcategorie = res
    })
  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  readUrl(event: any, i) {
    if (event.target.files.length === 0)
      return;
    //Image upload validation
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    // Image upload
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.url[i].img = reader.result.toString();
      console.log("img", this.url[i].img)

    }
  }

  onDeletItem(itemId: number): void {
    console.log(itemId);
  }
  onDeletePromo(promoId: number): void {
    console.log(promoId);
  }
  public onOpenModal(itemId: number): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.itemService.deleteItem(itemId).subscribe(
          (reponse: void) => {
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            console.log(reponse);
            this.getallitems();
          },
        );

      }
    })
  }


  getAllMarques() {
    this.MarqueService.getMarque().subscribe(res => {
      console.log(res)
      this.listmarque = res
      console.log("list marque", this.listmarque)
    })
  }

  calculprixprpmo() {
    this.x = Number (this.addFormItem.value.prixreel - ((this.addFormItem.value.prixreel/100) * this.addFormItem.value.remise)) 
    this.addFormItem.value.prixpromo = this.x.toFixed(2)
    this.y = this.x
    this.addFormItem.patchValue({
    prixpromo:  this.y
    })



  }

  calculprixprpmo1() {
    this.x = Number (this.addFormItem1.value.prixreel - ((this.addFormItem1.value.prixreel/100) * this.addFormItem1.value.remise)) 
    this.addFormItem1.value.prixpromo = this.x.toFixed(2)
    this.y = this.x
    this.addFormItem1.patchValue({
    prixpromo:  this.y
    })


  }


}
