import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LivraisonComponent } from '../livraisons/livraison/livraison.component';
import { OrdersComponent } from './orders/orders.component';
import { PaiementComponent } from '../paiements/paiement/paiement.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'orders',
        component: OrdersComponent,
        data: {
          title: "Orders List",
          breadcrumb: "Orders"
        }
      },
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesRoutingModule { }
