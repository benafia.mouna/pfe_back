import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { CommandeService } from 'src/service/commandeService';
import { ModePaiementService } from 'src/service/modepaiementService';
import { PromoService } from 'src/service/promoService';
import { UserService } from 'src/service/userService';
import Swal from 'sweetalert2';
import { orderDB } from "../../../shared/tables/order-list";
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  order: any = []
  public temp = [];
  commande: any
  listcommande : any
  deleteordre:any
  listmodepaiement:any
  email = localStorage.getItem("email")
  user: any

  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  constructor(private promoService: PromoService, private commandeservice: CommandeService, private userService: UserService , private modepaiementservice : ModePaiementService) {
    // this.order = orderDB.list_order;
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.order = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  ngOnInit() {
   
    this.getalluser()

    this.findUserByEmail()


  }
    

  findUserByEmail() {
    this.userService.findUser(this.email).subscribe((res: any) => {
      ///  this.user = res
      console.log(" of user ", res)
      console.log("role of user", res.roles[0]?.name)
    
      this.promoService.getCommandevendeur(res.id).subscribe(async (res:any) => {
        this.order = res
        console.log("orders of user 1 ", this.order)
     
        this.order = await this.order.map(item => 
         {
          return  {
            "Order id" : item?.id,
            "Promotion": item?.promos.map(item=> { 
              
              let  url ='http://127.0.0.1:8887/'+item.img
              return `<img src=${url} class='img-30 mr-2'>`}
              
              ),
            "Payment Status": "<span class='badge badge-secondary'>"+item?.paiement?.status+"</span>",
            "Payment Method": "<span class='badge badge-secondary'>"+item?.paiement?.modePaiement?.name+"</span>",
            "Order Status": "<span class='badge badge-success'>"+item?.status+ "</span>",
            "Date": item?.date,
            "Total":item.total +""+"TND",
            Actions: //or something
            {
              title:'Detail',
              type:'html',
              valuePrepareFunction:(cell,row)=>{
                return `<a title="See Detail Product "href="Your api key or something/${row.Id}"> <i class="ion-edit"></i></a>`
              },
              filter:false       
            },
            Id: { //this Id to use in ${row.Id}
              title: 'ID',
              type: 'number'
            },
          }
         }
         
        
        )
        console.log("orders of user 2", this.order)

      })
    })
  }
  
  
  getalluser() {
    this.userService.getUsers().subscribe((res: any) => {
      this.user = res
    })
  }
  onSelect({selected}) {
    console.log('Array of selected vehicles', selected);
  }

}


