import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModeLivraisonService } from 'src/service/modelivraison';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-mode-livraison',
  templateUrl: './mode-livraison.component.html',
  styleUrls: ['./mode-livraison.component.scss']
})
export class ModeLivraisonComponent implements OnInit {
  public closeResult: string;
  result : any
  listmodeliv: any;
  public addForm: FormGroup;
  public addForm1: FormGroup;
  search_name:any
  p: number = 1;
  deletModeliv: any;




  constructor(private modalService: NgbModal, private modelivraisonService : ModeLivraisonService, private form: FormBuilder ) {
    
  }


  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  
  open1(content, modeliv: any) {
    this.addForm1.patchValue({
      name: modeliv.name,
      id: modeliv.id
    })
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit() {

    this.addForm = this.form.group({
      name: ['', Validators.required],
    
    });
    this.addForm1 = this.form.group({
      name: ['', Validators.required],
      id: ['', Validators.required],
    
    });

    this.getallmodelivraison()
    

  
  }
  getallmodelivraison() {

    this.modelivraisonService.getModeLivraison().subscribe(res => {
      console.log(res)
      this.listmodeliv = res
      console.log("list mode livraison", this.listmodeliv)
    })
  }


  onAddModeLivraison() {

    this.modelivraisonService.addModeLivraison(this.addForm.value).subscribe(
      (reponse: any) => {
        if(reponse != null){
        console.log(reponse);
        Swal.fire({
     
          icon: 'success',
          title: 'Add has been saved',
          showConfirmButton: false,
          timer: 1500
        })
        this.getallmodelivraison();
        
      }
      else{
        Swal.fire({
          title: 'Delivery method already exist !'
        })
      }
    },
  );
  this.addForm.reset();
}


  onUpdateModelivraison() {
    this.modelivraisonService.updateModeLivraison(this.addForm1.value, this.addForm1.value.id).subscribe(
      (reponse: any) => {
        if(reponse != null){
        console.log(reponse);
        Swal.fire({
          icon: 'success',
          title: 'Updated successfully !',
          showConfirmButton: false,
          timer: 2000
        })
        this.getallmodelivraison();
      }
      else{
        Swal.fire({
          title: 'Delivery method already exist !'
        })
      }
    },
  );
}

  public onOpenModal(modelivId: number): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.modelivraisonService.deleteModeLivraison(modelivId).subscribe(
          (reponse: void) => {
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            console.log(reponse);
            this.getallmodelivraison();
          },
        );

      }
    })
  }

  onDeletModeliv(modelivId: number): void {
    console.log(modelivId);

  }

}
