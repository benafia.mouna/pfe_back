import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Permission } from 'src/model/permission';
import { PermissionService } from 'src/service/permissionService';
@Component({
  selector: 'app-list-permission',
  templateUrl: './list-permission.component.html',
  styleUrls: ['./list-permission.component.scss']
})
export class ListPermissionComponent implements OnInit {
  public closeResult: string;
  listpermission: any;
  permission: Permission;
  constructor(private modalService: NgbModal, private permissionService: PermissionService, private form: FormBuilder) { }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  ngOnInit() {

    // this.addFormPermission = this.form.group({
    //   name: ['', Validators.required]
  
    // });
    this.getallpermissions()
  }
  getallpermissions() {
    this.permissionService.getPermission().subscribe(res => {
      console.log(res)
      this.listpermission = res
      console.log("list permission", this.listpermission)
    })
  }

  }


