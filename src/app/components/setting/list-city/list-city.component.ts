import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CityService } from 'src/service/cityservice';
import { CountryService } from 'src/service/countryservice';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-city',
  templateUrl: './list-city.component.html',
  styleUrls: ['./list-city.component.scss']
})
export class ListCityComponent implements OnInit {

  public closeResult: string;
  result : any
  listcity: any;
  public addForm: FormGroup;
  public addForm1: FormGroup;
  search_name:any
  p: number = 1;
  deletCity: any;
  listcountry:any


  constructor(private modalService: NgbModal,private countryService : CountryService, private cityService : CityService, private form: FormBuilder ) {
  
  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  
  open1(content, city: any) {
    this.addForm1.patchValue({
      id_country : city.country.id,
      name: city.name,
      id: city.id
    })
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit() {

    this.addForm = this.form.group({
      id_country: ['',Validators.required],
      name: ['', Validators.required],
     
    });
    this.addForm1 = this.form.group({
      name: ['', Validators.required],
      id: ['', Validators.required],
      id_country: ['',Validators.required],
    
    });

    this.getallcity()
    this.getallcountry()
   
  
  }
  getallcity() {

    this.cityService.getCity().subscribe(res => {
      console.log(res)
      this.listcity = res
      console.log("list city", this.listcity)
    })
  }
  getallcountry() {

    this.countryService.getCountry().subscribe(res => {
      console.log(res)
      this.listcountry = res
      console.log("list country", this.listcountry)
    })
  }

  onAddCity() {

    this.cityService.addCity(this.addForm.value,this.addForm.value.id_country).subscribe(
      (reponse: any) => {
        if(reponse != null){
        console.log(reponse);
        Swal.fire({
          icon: 'success',
          title: 'Add has been saved',
          showConfirmButton: false,
          timer: 1500
        })
        this.getallcity();
        this.addForm.reset();
      }else{
        Swal.fire({
          title: 'City already exist !'
        })
      }}
    )
   
  }

  onUpdateCity() {
    this.cityService.updateCity(this.addForm1.value, this.addForm1.value.id_country,this.addForm1.value.id).subscribe(
      (reponse: any) => {
        if(reponse != null){
        console.log(reponse);
        Swal.fire({
          icon: 'success',
          title: 'Updated successfully !',
          showConfirmButton: false,
          timer: 2000
        })
        this.getallcity();}else{
          Swal.fire({
            title: 'City already exist !'
          })
        }

      },
    );
  }

  public onOpenModal(cityId: number): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.cityService.deleteCity(cityId).subscribe(
          (reponse: void) => {
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            console.log(reponse);
            this.getallcity();
          },
        );

      }
    })
  }

  onDeletCity(cityId: number): void {
    console.log(cityId);

  }

}
