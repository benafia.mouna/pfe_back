import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Validation from 'src/helper/match';
import { User } from 'src/model/user';
import { UserService } from 'src/service/userService';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  listuser: any;
  submitted = false;
  user: User;
  fileToUpload: Array<File> = [];
  userupdate: FormGroup;
  changepassword: FormGroup;
  pourcenForm : FormGroup;
  url: any;
  discount: any;
  email = localStorage.getItem("email")
  constructor(private userservice: UserService, private form: FormBuilder 
    ) { }

  ngOnInit() {
    this.getallusers()
    this.calcul()
    this.findUserByEmail()


    this.pourcenForm = this.form.group({
      Oldpourcentage:['',Validators.required],
      
    })

    this.userupdate = this.form.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      imageUrl: ['', Validators.required],
      phone: ['', Validators.required],
      type: ['', Validators.required],
      mat_fiscale: ['', Validators.required],
      cin: ['', Validators.required],
      adresse: ['', Validators.required],
      newpassword: [''],
      image_url: ['']
    })
    this.changepassword = this.form.group({
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    }, {
      validators: Validation.match('password', 'confirmPassword')

    });


  }

  handleFileInput(files: any) {
    this.fileToUpload = <Array<File>>files.target.files;
    console.log("file To Upload " + this.fileToUpload);
  }
  getallusers() {

    this.userservice.getUsers().subscribe(res => {
      console.log(res)
      this.listuser = res
    })
  }
  get f() { return this.changepassword.controls; }

  updateDiscount(){
    this.userservice.scraping().subscribe()
    this.userservice.updatepourcentage(this.pourcenForm.value.Oldpourcentage).subscribe(res =>{
      this.discount = res
      this.pourcenForm.get("Oldpourcentage").setValue(res)
    })
    Swal.fire({
      icon: 'success',
      title: 'Updated successfully !',
      showConfirmButton: false,
      timer: 2000
    })
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      }
    }
  }
  calcul(){
    this.userservice.findDiscount("1").subscribe(
      (res=>{
        this.discount=res
        this.pourcenForm.get("Oldpourcentage").setValue(res)
      })
      
     
    )
  }




  findUserByEmail() {
    this.userservice.findUser(this.email).subscribe(res => {
      this.user = res
      this.url = 'http://127.0.0.1:8887/' + this.user?.imageUrl;
      console.log("user", this.user)
      this.userupdate.patchValue({
        firstName: this.user?.firstName,
        lastName: this.user?.lastName,
        email: this.user?.email,
        phone: this.user?.phone,
        type: this.user?.type,
        mat_fiscale: this.user?.mat_fiscale,
        cin: this.user?.cin,
        adresse: this.user?.adresse,
        image_url: this.user?.imageUrl


      })


    })
  }
  isvendeur() {
    return this.user?.roles[0]?.name == "VENDEUR" ? true : false
  }
  isadmin() {
    return this.user?.roles[0]?.name == "ADMIN" ? true : false
  }

  isentreprise() {
    return this.user?.type == "Entreprise" ? true : false
  }
  isparticulier() {
    return this.user?.type == "Particulier" ? true : false
  }


  updatepassword() {
    this.submitted = true;
    if (this.changepassword.invalid) {
      return;
    }
    console.log('the new password most be :',this.changepassword.value.password)
    this.userservice.updatePassword(this.user.id, this.changepassword.value.password).subscribe((res: any) => {
      console.log(res)
      Swal.fire({
     
        icon: 'success',
        title: 'Password updated',
        showConfirmButton: false,
        timer: 1500
      })
    })
  }


  upadateuser() {
    let formdata = new FormData()
    formdata.append("firstName", this.userupdate.value.firstName)
    formdata.append("lastName", this.userupdate.value.lastName)
    formdata.append("email", this.userupdate.value.email)
    formdata.append("phone", this.userupdate.value.phone)
    formdata.append("type", this.userupdate.value.type)
    formdata.append("mat_fiscale", this.userupdate.value.mat_fiscale)
    formdata.append("cin", this.userupdate.value.cin)
    formdata.append("adresse", this.userupdate.value.adresse)

    if (this.fileToUpload.length > 0) {
      formdata.append("file", this.fileToUpload[0]);

      this.userservice.updateUser(formdata, this.user.id).subscribe((res: any) => {
        console.log(res)
        // localStorage.setItem("email",JSON.stringify(res.email))
        // window.location.href="http://localhost:4200/settings/profile"
        Swal.fire({
          icon: 'success',
          title: 'Updated successfully !',
          showConfirmButton: false,
          timer: 2000
        })
        

      })
      location.reload();
    } else {

      formdata.append("imageUrl", this.userupdate.value.image_url)

      this.userservice.noupdateUser(formdata, this.user.id).subscribe((res: any) => {
        console.log(res)
        // localStorage.setItem("email",JSON.stringify(res.email))
        // window.location.href="http://localhost:4200/settings/profile"
        Swal.fire({
          icon: 'success',
          title: 'Updated successfully !',
          showConfirmButton: false,
          timer: 2000
        })
        
      })
      location.reload();
    }

  }



}
