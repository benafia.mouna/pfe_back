import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GererPermissionComponent } from './gerer-permission.component';

describe('GererPermissionComponent', () => {
  let component: GererPermissionComponent;
  let fixture: ComponentFixture<GererPermissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GererPermissionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GererPermissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
