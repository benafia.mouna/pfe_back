
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RoleService } from 'src/service/roleService';
import { PermissionService } from 'src/service/permissionService';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/service/userService';
import { resetFakeAsyncZone } from '@angular/core/testing';

@Component({
  selector: 'app-gerer-permission',
  templateUrl: './gerer-permission.component.html',
  styleUrls: ['./gerer-permission.component.scss']
})
export class GererPermissionComponent implements OnInit {
  public closeResult: string;
  public accountForm: FormGroup;
  public permissionForm: FormGroup;
  listrole: any;
  listacheteur:any;
  listpermissionadmin: any;
  listpermissionvendeur: any;
  listpermissionacheteur: any;
  listadmin:any;

  listvendeur:any
  ids: any;
  a:Array<string> = [];
  b:Array<string> = [];
  c:Array<string> = [];
  d:Array<string> = [];
  e:Array<string> = [];
  f:Array<string> = [];
  vendeur:boolean=false;
  listpermission1:any
  addFormRole: FormGroup;
  addFormRole1: FormGroup;
  addFormRole2:FormGroup;
  addFormRole3:FormGroup;
  addFormRole4:FormGroup;
  addFormRole5:FormGroup;

  constructor(private formBuilder: FormBuilder, private modalService: NgbModal, private roleservice: RoleService, private permissionService: PermissionService , private userService: UserService) {

    this.createPermissionForm();
  }

  createPermissionForm() {
    this.permissionForm = this.formBuilder.group({
    })
  }


  
  open1(content) {
      // this.addFormRole.patchValue({
    //   name:role.name,
    //   // roles_permission:role. roles_permission

    // })
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  ngOnInit() {
    this.addFormRole = this.formBuilder.group({
      name: ['', Validators.required],
      roles_permission: ['', Validators.required],

    });
    this.addFormRole1 = this.formBuilder.group({
      name: ['', Validators.required],
      roles_permission: ['', Validators.required],

    });
    this.addFormRole2 = this.formBuilder.group({
      name: ['', Validators.required],
      roles_permission: ['', Validators.required],

    });
    this.addFormRole3 = this.formBuilder.group({
      name: ['', Validators.required],
      roles_permission: ['', Validators.required],

    });
    this.addFormRole4 = this.formBuilder.group({
      name: ['', Validators.required],
      roles_permission: ['', Validators.required],

    });
    this.addFormRole5 = this.formBuilder.group({
      name: ['', Validators.required],
      roles_permission: ['', Validators.required],

    });


    this.getAllRole()
     this.getAllPermissionadmin()
    this.getAllPermissionvendeur()
    this.getAllPermissionacheteur()
    
    
  }

  
  getAllRole () {
    this.roleservice.getRole().subscribe(res => {
      console.log("RES C EST ICI ", res)
      this.listrole = res
      this.listadmin=this.listrole.filter((el:any)=>el.name=='ADMIN')
      this.listvendeur=this.listrole.filter((el:any)=>el.name=='VENDEUR')
      this.listacheteur=this.listrole.filter((el:any)=>el.name=='CLIENT')

    })
  }

  getAllPermissionadmin() {
    this.permissionService.getPermission().subscribe((res:any) => {
      for (let i = 0; i < res.length; i++) {
        this.a.push(res[i].name)
      }
      for (let i = 0; i < this.listadmin[0]?.permissions.length; i++) {
        this.b.push(this.listadmin[0]?.permissions[i].name)
      }
      this.listpermissionadmin  = this.a.filter(val => !this.b.includes(val));
      console.log("admin",this.listpermissionadmin)
   }
    )
  }
  getAllPermissionacheteur() {
    this.permissionService.getPermission().subscribe((res:any) => {
      for (let i = 0; i < res.length; i++) {
        this.c.push(res[i].name)
      }
      for (let i = 0; i < this.listacheteur[0]?.permissions.length; i++) {
        this.d.push(this.listacheteur[0]?.permissions[i].name)
      }
      this.listpermissionacheteur  = this.c.filter(val => !this.d.includes(val));
      console.log("acheteur",this.listpermissionacheteur)
   }
    )
  }

  getAllPermissionvendeur () {
    this.permissionService.getPermission().subscribe((res:any) => {
      for (let i = 0; i < res.length; i++) {
        this.e.push(res[i].name)
      }
      for (let i = 0; i < this.listvendeur[0]?.permissions.length; i++) {
        this.f.push(this.listvendeur[0]?.permissions[i].name)
      }
      this.listpermissionvendeur  = this.e.filter(val => !this.f.includes(val));
      console.log("vendeur",this.listpermissionvendeur)
    
   }
    )
    
  }
  
  AjoutAdminRolePermission() {
    this.ids = this.addFormRole.value.roles_permission
    
    console.log("ids", this.ids)
    
    this.roleservice.addpermission( "ADMIN", this.ids).subscribe(res => {
      this.listrole = res
      console.log(res)
      this.getAllRole();
      this.addFormRole.reset();
      window.location.reload();
    },
    );
  }
  RemoveAdminRolePermission(){
    this.ids = this.addFormRole1.value.roles_permission
     this.roleservice.removepermission("ADMIN",this.ids).subscribe(
       (reponse: any) => {
         console.log(reponse);
    
       },
     );
   }
  AjoutVendeurRolePermission() {
    this.ids = this.addFormRole2.value.roles_permission
    
    console.log("ids", this.ids)
    
    this.roleservice.addpermission("VENDEUR", this.ids).subscribe(res => {
      this.listrole = res
      console.log(res)
      this.getAllRole();
      this.addFormRole.reset();
      window.location.reload();
    },
    );
  }
  RemoveVendeurRolePermission(){
    this.ids = this.addFormRole3.value.roles_permission
     this.roleservice.removepermission("VENDEUR",this.ids).subscribe(
       (reponse: any) => {
         console.log(reponse);
         
    
       },
     );
     window.location.reload();
   }
  AjoutAcheteurRolePermission() {
    this.ids = this.addFormRole4.value.roles_permission
    
    console.log("ids", this.ids)
    
    this.roleservice.addpermission( "CLIENT", this.ids).subscribe(res => {
      this.listrole = res
      console.log(res)
      this.getAllRole();
      this.addFormRole.reset();
      window.location.reload();
    },
    );
  }
     
   RemoveAcheteurRolePermission(){
    this.ids = this.addFormRole5.value.roles_permission
    console.log('test that : ',this.ids)
     this.roleservice.removepermission("CLIENT",this.ids).subscribe(
       (reponse: any) => {
        // console.log(reponse);
    
       },
     );
     window.location.reload();
   }
 
}