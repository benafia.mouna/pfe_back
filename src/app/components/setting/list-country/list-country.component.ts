import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CountryService } from 'src/service/countryservice';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-country',
  templateUrl: './list-country.component.html',
  styleUrls: ['./list-country.component.scss']
})
export class ListCountryComponent implements OnInit {
  public closeResult: string;
  result : any
  listcountry: any;
  fileToUpload: Array<File> = [];
  public addForm: FormGroup;
  public addForm1: FormGroup;
  search_name:any
  p: number = 1;
  deletCountry:any


  constructor(private modalService: NgbModal, private countryService : CountryService, private form: FormBuilder ) {


  }


  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  
  open1(content, country: any) {
    this.addForm1.patchValue({
      name: country.name,
      id: country.id
    })
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit() {

    this.addForm = this.form.group({
      name: ['', Validators.required],
 
    });
    this.addForm1 = this.form.group({
      name: ['', Validators.required],
      id: ['', Validators.required],

    });

    this.getallcountry()
  
  
  }
  getallcountry() {

    this.countryService.getCountry().subscribe(res => {
      console.log(res)
      this.listcountry = res
      console.log("list country", this.listcountry)
    })
  }
  onAddCountry() {

    this.countryService.addCountry(this.addForm.value).subscribe(
      (reponse: any) => {
        if(reponse != null){
        console.log(reponse);
        Swal.fire({
     
          icon: 'success',
          title: 'Add has been saved',
          showConfirmButton: false,
          timer: 1500
        })
        this.getallcountry();
        this.addForm.reset();
      }else{
        Swal.fire({
          title: 'Country already exist !'
        })
      }}
    )
   
  }

  onUpdateCountry() {
    this.countryService.updateCountry(this.addForm1.value, this.addForm1.value.id).subscribe(
      (reponse: any) => {
        console.log(reponse);
        Swal.fire({
          icon: 'success',
          title: 'Updated successfully !',
          showConfirmButton: false,
          timer: 2000
        })
        this.getallcountry();
      },
    );
  }

  public onOpenModal(countryId: any): void {
    console.log('aaaaaaaaaaaa',countryId);
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.countryService.deleteCountry(countryId).subscribe(
          (reponse: void) => {
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            console.log(reponse);
            this.getallcountry();
          },
        );

      }
    })
  }

  onDeleteCountry(subcategorieId:number ):void{ 
    console.log(subcategorieId);
    this.countryService.deleteCountry(subcategorieId).subscribe(
      (reponse: void) => {
        console.log(reponse);
        this.getallcountry();
      },
    );
    }

}
