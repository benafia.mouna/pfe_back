import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryComponent } from './category/category.component';
import { MarqueComponent } from './marque/marque.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { ProfileComponent } from './profile/profile.component';
import { ListCityComponent } from './list-city/list-city.component';
import { ListCountryComponent } from './list-country/list-country.component';
import { ModeLivraisonComponent } from './mode-livraison/mode-livraison.component';
import { ModePaiementComponent } from './mode-paiement/mode-paiement.component';
import { ListPermissionComponent } from './list-permission/list-permission.component';
import { GererPermissionComponent } from './gerer-permission/gerer-permission.component';




const routes: Routes = [
  {
    path: 'profile',
    component: ProfileComponent,
    data: {
      title: "Profile",
      breadcrumb: "Profile"
    }
  },
  {
    path: 'categorie',
    component: CategoryComponent,
    data: {
      title: "Category List ",
      breadcrumb: "Category"
    }
  },
  {
    path: 'sub-categorie',
    component: SubCategoryComponent,
    data: {
      title: "Sub Category List",
      breadcrumb: "Sub Category"
    }
  },
  {
    path: 'marque',
    component: MarqueComponent,
    data: {
      title: "Mark List",
      breadcrumb: "Marque"
    }
  },
  {
    path: 'country',
    component: ListCountryComponent,
    data: {
      title: "Country List",
      breadcrumb: "Country"
    }
  },
  {
    path: 'city',
    component: ListCityComponent,
    data: {
      title: "City List",
      breadcrumb: "City List"
    }
  },
  {
    path: 'mode-livraison',
    component: ModeLivraisonComponent,
    data: {
      title: "Delivery method List",
      breadcrumb: "Delivery method List"
    }
  },
  {
    path: 'mode-paiement',
    component: ModePaiementComponent,
    data: {
      title: "Payment method List ",
      breadcrumb: "Payment method List"
    }
  },
  {
    path: 'gerer-permission',
    component: GererPermissionComponent,
    data: {
      title: "Manage Permission",
      breadcrumb: "Manage Permission"
    }
  },
  {
    path: 'list-permission',
    component: ListPermissionComponent,
    data: {
      title: "Permission List",
      breadcrumb: "List Permission"
    }
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingRoutingModule { }
