import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SettingRoutingModule } from './setting-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { SharedModule } from '../../shared/shared.module';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { CategoryComponent } from './category/category.component';
import { MarqueComponent } from './marque/marque.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FullCalendarModule } from '@fullcalendar/angular';
import { GalleryModule } from '@ks89/angular-modal-gallery';
import { ListCityComponent } from './list-city/list-city.component';
import { ListCountryComponent } from './list-country/list-country.component';
import { ModePaiementComponent } from './mode-paiement/mode-paiement.component';
import { ModeLivraisonComponent } from './mode-livraison/mode-livraison.component';
import { ListPermissionComponent } from './list-permission/list-permission.component';
import { GererPermissionComponent } from './gerer-permission/gerer-permission.component';


@NgModule({
  declarations: [SubCategoryComponent,CategoryComponent,ProfileComponent,MarqueComponent,ListCityComponent,ListCountryComponent,ModePaiementComponent,ModeLivraisonComponent,GererPermissionComponent , ListPermissionComponent],
  imports: [
    CommonModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    SettingRoutingModule,
    SharedModule,
    Ng2SmartTableModule,
    NgbModule,
    FullCalendarModule,
    Ng2SearchPipeModule,
    GalleryModule.forRoot()
  ]
})
export class SettingModule { }
