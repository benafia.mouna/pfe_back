import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MarqueService } from 'src/service/marqueService';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { AuthService } from 'src/service/authService';
import { DesignutilityServiceService } from 'src/app/designutility-service.service';


@Component({
  selector: 'app-marque',
  templateUrl: './marque.component.html',
  styleUrls: ['./marque.component.scss']
})
export class MarqueComponent implements OnInit {
  public addFormMarque: FormGroup;
  public addFormMarque1 : FormGroup;
  listmarque: any;
  fileToUpload:Array<File>=[];
  public closeResult: string;
  deletMarque : any;
  imageUrl = "" ;
  file_error = "" ;
  p: number = 1;
 result : any

  search_name:any
  constructor(private formBuilder: FormBuilder, private modalService: NgbModal, private marqueService: MarqueService , private auth :AuthService) { }


  ngOnInit() {
  
    this.addFormMarque = this.formBuilder.group({
      libelle: ['', Validators.required],
      img: ['', Validators.required],

    });
    this.addFormMarque1 = this.formBuilder.group({
      id: ['' , Validators.required],
      libelle: ['' , Validators.required],
      img : ['' , Validators.required],
    })
    this.getAllMarques()
   

  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  open1(content,marque:any){
    this.addFormMarque1.patchValue({
      libelle:marque.libelle,
      img:marque.img,
      id:marque.id
    }) 
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
  
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  onUpdateMarque() {
    let formdata= new FormData()
    formdata.append("libelle",this.addFormMarque1.value.libelle)

    if(this.fileToUpload.length>0) {
    formdata.append("file",this.fileToUpload[0]);
    this.marqueService.modifimage(formdata ,this.addFormMarque1.value.id).subscribe((res:any)=>{
      console.log(res)
      Swal.fire({
        icon: 'success',
        title: 'Modification terminée !',
        showConfirmButton: false,
        timer: 2000
      })
    })
    location.reload();
  }  else {

      formdata.append("img", this.addFormMarque1.value.img)

    this.marqueService.modifnoimage(formdata, this.addFormMarque1.value.id).subscribe((res:any)=>{
      console.log(res)
      Swal.fire({
        icon: 'success',
        title: 'Updated successfully !',
        showConfirmButton: false,
        timer: 2000
      })
      location.reload();
    })
    }
    
  }

  handleFileInput(files: any){
    this.fileToUpload=<Array<File>>files.target.files;
    console.log("file To Upload "+ this.fileToUpload);
  }
  onAddMarque() {
  let formdata= new FormData()
  formdata.append("libelle",this.addFormMarque.value.libelle)
  formdata.append("file",this.fileToUpload[0]);
    this.marqueService.addMarque(formdata).subscribe(
      (reponse: any) => {
     if(reponse != null){
        console.log(reponse);
        Swal.fire({
          icon: 'success',
          title: 'Add has been saved',
          showConfirmButton: false,
          timer: 1500
        })
        this.getAllMarques();
        this.addFormMarque.reset();
      }else{
        Swal.fire({
          title: 'Mark already exist !'
        })
      }}
    )
  
  }

  getAllMarques() {
    this.marqueService.getMarque().subscribe(res => {
      console.log(res)
      this.listmarque = res
      console.log("list marque", this.listmarque)
    })
  }

  
public onOpenModal(marqueId:number): void { 
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.isConfirmed) {
      this.marqueService.deleteMarque(marqueId).subscribe(
        (reponse: void) => {
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          console.log(reponse);
          this.getAllMarques();
        },
      );
      
    }
  })
}


onDeletMarque(marqueId:number ):void{ 
  console.log(marqueId);
    
}
}

