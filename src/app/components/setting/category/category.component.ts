import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DesignutilityServiceService } from 'src/app/designutility-service.service';
import { categoryDB } from 'src/app/shared/tables/category';
import { CategorieService } from 'src/service/categorieService';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  public closeResult: string;
  result : any
  public categories = []
  listcategorie: any;
  fileToUpload: Array<File> = [];
  public addForm: FormGroup;
  public addForm1: FormGroup;
  search_name:any
  p: number = 1;
  // public SERVER_API_URL = "http://192.168.1.18:8090/resources/MounaFolder/videos";

  public categoriies: any;
  deletCategorie: any;
  imageUrl = "";
  file_error = "";



  constructor(private modalService: NgbModal, private categorieService : CategorieService, private form: FormBuilder ) {
    this.categories = categoryDB.category;

  }


  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  //   public config1: DropzoneConfigInterface = {


  //     url: "http://192.168.1.18:8090/resources/MounaFolder/videos",
  //     headers: {
  //       Authorization: `Bearer ${localStorage.getItem('token')}`,
  //       'Cache-Control': null,
  //       'X-Requested-With': null
  //     },

  //     clickable: true,
  //     maxFiles: 3,
  //     autoReset: null,
  //     errorReset: null,
  //     cancelReset: null
  //   };
  //   maxFile(event: any) {
  //     // this.resetDropzoneUploads();
  //     event.previewElement.remove();
  //   }
  //   /**
  //  * @param args
  //  * Handel success upload
  //  */
  //   public onUploadSuccess(args: any): void {
  //     console.log(args);
  //     this.imageUrl = args[1].message;
  //     console.log(args);
  //     this.file_error = `<div class="text-success f-w-700"> Image corrrect </div>`;
  //   }
  //   public onUploadError(args: any): void {
  //     console.log(args);
  //     if (args[1] === 'Invalid dimension.') {
  //       this.file_error = `<div class="text-danger"> Résolution doite etre : 1920x1080 px </div>`;
  //     }
  //   }
  open1(content, categorie: any) {
    this.addForm1.patchValue({
      name: categorie.name,
      description: categorie.description,
      id: categorie.id
    })
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit() {

    this.addForm = this.form.group({
      name: ['', Validators.required],
      description: [''],
      // img: ['', Validators.required],
    });
    this.addForm1 = this.form.group({
      name: ['', Validators.required],
      id: ['', Validators.required],
      description: [''],
      // img: ['', Validators.required],
    });

    this.getallcategorie()
    

  
  }
  getallcategorie() {

    this.categorieService.getCategorie().subscribe(res => {
      console.log(res)
      this.listcategorie = res
      console.log("list categorie", this.listcategorie)
    })
  }

  // handleFileInput(files: any) {
  //   this.fileToUpload = <Array<File>>files.target.files;
  //   console.log("file To Upload " + this.fileToUpload);
  // }
  onAddCategorie() {
    // let formdata = new FormData()
    // formdata.append("name", this.addForm.value.name)
    // formdata.append("file", this.fileToUpload[0]);
    this.categorieService.addCategorie(this.addForm.value).subscribe(
      (reponse: any) => {
        if(reponse != null){
        console.log(reponse);
        Swal.fire({
     
          icon: 'success',
          title: 'Add has been saved',
          showConfirmButton: false,
          timer: 1500
        })
        this.getallcategorie();
        this.addForm.reset();
      }else{
        Swal.fire({
          title: 'Category already exist !'
        })
      }}
    )
   
  }

  onUpdateCategorie() {
    this.categorieService.updateCategorie(this.addForm1.value, this.addForm1.value.id).subscribe(
      (reponse: any) => {
         if(reponse != null){
        console.log(reponse);
        Swal.fire({
          icon: 'success',
          title: 'Updated successfully !',
          showConfirmButton: false,
          timer: 2000
        })
        this.getallcategorie();}
        else{
          Swal.fire({
            title: 'Category already exist !'
          })
        }
      },
    );
  }

  public onOpenModal(categorieId: number): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.categorieService.deleteCategorie(categorieId).subscribe(
          (reponse: void) => {
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            console.log(reponse);
            this.getallcategorie();
          },
        );

      }
    })
  }

  onDeletCategorie(categorieId: number): void {
    console.log(categorieId);

  }
}