import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModePaiementService } from 'src/service/modepaiementService';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-mode-paiement',
  templateUrl: './mode-paiement.component.html',
  styleUrls: ['./mode-paiement.component.scss']
})
export class ModePaiementComponent implements OnInit {

  public closeResult: string;
  result : any
  listmodepaiement: any;
  public addForm: FormGroup;
  public addForm1: FormGroup;
  search_name:any
  p: number = 1;
  deletModepaiement: any;




  constructor(private modalService: NgbModal, private modepaiementservice : ModePaiementService, private form: FormBuilder ) {
    
  }


  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  
  open1(content1, modepaiement: any) {
    this.addForm1.patchValue({
      name: modepaiement.name,
      id: modepaiement.id
    })
    this.modalService.open(content1, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit() {

    this.addForm = this.form.group({
      name: ['', Validators.required],
    
    });
    this.addForm1 = this.form.group({
      name: ['', Validators.required],
      id: ['', Validators.required],
    
    });

    this.getallmodepaiement()
    

  
  }
  getallmodepaiement() {

    this.modepaiementservice.getModePaiement().subscribe(res => {
      console.log(res)
      this.listmodepaiement = res
      console.log("list mode paiement", this.listmodepaiement)
    })
  }


  onAddModePaiement() {

    this.modepaiementservice.addModePaiement(this.addForm.value).subscribe(
      (reponse: any) => {
        if(reponse != null){
        console.log(reponse);
        Swal.fire({
     
          icon: 'success',
          title: 'Add has been saved',
          showConfirmButton: false,
          timer: 1500
        })
        this.getallmodepaiement();
        this.addForm.reset();
      }
      else{
        Swal.fire({
          title: 'Payment method already exist !'
        })
      }
    },
  );
}

  onUpdateModepaiement() {
    
    this.modepaiementservice.updateModePaiement(this.addForm1.value, this.addForm1.value.id).subscribe(

      (reponse: any) => {
        if(reponse != null){
        console.log(reponse);
        Swal.fire({
          icon: 'success',
          title: 'Updated successfully !',
          showConfirmButton: false,
          timer: 2000
        })
        this.getallmodepaiement();
      }
      else{
        Swal.fire({
          title: 'Payment method already exist !'
        })
      }
    },
  );
}

  public onOpenModal(modepaiementId: number): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.modepaiementservice.deleteModePaiement(modepaiementId).subscribe(
          (reponse: void) => {
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            console.log(reponse);
            this.getallmodepaiement();
          },
        );

      }
    })
  }

  onDeletModepaiement(modepaiementId: number): void {
    console.log(modepaiementId);

  }
}
