import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

import { CategorieService } from 'src/service/categorieService';
import { SubcategorieService } from 'src/service/subcategorieService';


@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.scss']
})
export class SubCategoryComponent implements OnInit {
  public closeResult: string;
  result : any;
  public digital_sub_categories = []
  public addFormSubCategorie : FormGroup;
  addFormSubCategorie1 : FormGroup;
  listcategorie : any;
  fileToUpload:Array<File>=[];
  listsubcategorie : any;
  deletesubcategorie : any;
  imageUrl = "" ;
  file_error = "" ;
  p: number = 1;
  search_name : any
  constructor(private modalService: NgbModal, private form:FormBuilder , private subcategorieService :SubcategorieService  , private categorieService : CategorieService ) {
    
  }

  ngOnInit() {

    this.addFormSubCategorie = this.form.group({
      idcategorie: ['',Validators.required],
      name: ['', Validators.required],
      description: [''],
   
    
    });

    this.addFormSubCategorie1 = this.form.group({
      id :['',Validators.required],
      idcategorie: ['',Validators.required],
      name: ['', Validators.required],
      description: [''],
    });

    this.getallcategorie()
    this.getallsubcategorie()
   
 
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  open1(content1 : any, subcategorie : any) {
    this.addFormSubCategorie1.patchValue({
      idcategorie : subcategorie.categorie.id,
      name: subcategorie.name,
      description: subcategorie.description,
      id: subcategorie.id
    }) 
    this.modalService.open(content1, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  handleFileInput(files: any) {
    this.fileToUpload = <Array<File>>files.target.files;
    console.log("file To Upload " + this.fileToUpload);
  }

  getallsubcategorie() {
  
    this.subcategorieService.getSubcategorie().subscribe(res => {
      this.listsubcategorie = res
      console.log("list sub categorie", this.listsubcategorie)
    })
  }

  onAddSubCategorie(){
  this.subcategorieService.addSubCategorie(this.addFormSubCategorie.value,this.addFormSubCategorie.value.idcategorie).subscribe(
      (reponse: any) => {
        if(reponse != null){
        console.log(reponse);
        Swal.fire({
          icon: 'success',
          title: 'Add has been saved',
          showConfirmButton: false,
          timer: 2300
        })
    
        this.getallsubcategorie();
        this.addFormSubCategorie.reset();
      }else{
        Swal.fire({
          title: 'Sub category already exist !'
        })
      }}
    );
    
  }
  onUpdateSubCategorie(){
      this.subcategorieService.updateSubCategorie(this.addFormSubCategorie1.value,this.addFormSubCategorie1.value.idcategorie,this.addFormSubCategorie1.value.id ).subscribe(
        (reponse: any) => {
          console.log(reponse);
          Swal.fire({
            icon: 'success',
            title: 'Updated successfully !',
            showConfirmButton: false,
            timer: 2000
          })
          this.getallsubcategorie();
        },
      );
    }
  
    onDeleteSubCategorie(subcategorieId:number ):void{ 
console.log(subcategorieId);
this.subcategorieService.deleteSubCategorie(subcategorieId).subscribe(
  (reponse: void) => {
    console.log(reponse);
    this.getallsubcategorie();
  },
);
}
// public config1: DropzoneConfigInterface = {
    
//   url: "http://192.168.1.18:8090/resources/MounaFolder/videos",
//   headers: {
//     Authorization: `Bearer ${localStorage.getItem('token')}`,
//     'Cache-Control': null,
//     'X-Requested-With': null
//   },
//   clickable: true,
//   maxFiles: 3,
//   autoReset: null,
//   errorReset: null,
//   cancelReset: null
// };
// maxFile(event: any) {
//   // this.resetDropzoneUploads();
//   event.previewElement.remove();
// }
// public onUploadSuccess(args: any): void {
//   console.log(args);
//   this.imageUrl = args[1].message;
//   console.log(args);
//   this.file_error = `<div class="text-success f-w-700"> Image corrrect </div>`;
// }
// public onUploadError(args: any): void {
//   console.log(args);
//   if (args[1] === 'Invalid dimension.') {
//     this.file_error = `<div class="text-danger"> Résolution doite etre : 1920x1080 px </div>`;
//   }
// }
public onOpenModal(subcategorieId:number): void { 
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.isConfirmed) {
      this.subcategorieService.deleteSubCategorie(subcategorieId).subscribe(
        (reponse: void) => {
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
      
          this.getallsubcategorie();
        },
      );
      
    }
  })
}
getallcategorie() {
  this.categorieService.getCategorie().subscribe(res => {
    this.listcategorie = res
    console.log("list categorie",this.listcategorie)
  })
}

}
