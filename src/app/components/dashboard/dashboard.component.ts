import { Component, OnInit } from '@angular/core';
import { CommandeService } from 'src/service/commandeService';
import { PromoService } from 'src/service/promoService';
import { UserService } from 'src/service/userService';
import * as chartData from '../../shared/data/chart';
import { doughnutData, pieData } from '../../shared/data/chart';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  result: any
  public doughnutData = doughnutData;
  public pieData = pieData;
  listvendeur: any = []
  listcontact: any = []
  listcommande: any
  count2: any;
  testRole = localStorage.getItem("role");
  count: any;
  count1: any;
  count3: any;
  count4:any;
  count5:any;
  user: any;
  ordes: any = []
  listpromo: any = []
  listacheteur: any
  email = localStorage.getItem("email")
  constructor(private userService: UserService, private promoService: PromoService , private commandeService: CommandeService) {
    Object.assign(this, { doughnutData, pieData })
  }

  // doughnut 2
  public view = chartData.view;
  public doughnutChartColorScheme = chartData.doughnutChartcolorScheme;
  public doughnutChartShowLabels = chartData.doughnutChartShowLabels;
  public doughnutChartGradient = chartData.doughnutChartGradient;
  public doughnutChartTooltip = chartData.doughnutChartTooltip;

  public chart5 = chartData.chart5;


  // lineChart
  public lineChartData = chartData.lineChartData;
  public lineChartLabels = chartData.lineChartLabels;
  public lineChartOptions = chartData.lineChartOptions;
  public lineChartColors = chartData.lineChartColors;
  public lineChartLegend = chartData.lineChartLegend;
  public lineChartType = chartData.lineChartType;

  // lineChart
  public smallLineChartData = chartData.smallLineChartData;
  public smallLineChartLabels = chartData.smallLineChartLabels;
  public smallLineChartOptions = chartData.smallLineChartOptions;
  public smallLineChartColors = chartData.smallLineChartColors;
  public smallLineChartLegend = chartData.smallLineChartLegend;
  public smallLineChartType = chartData.smallLineChartType;

  // lineChart
  public smallLine2ChartData = chartData.smallLine2ChartData;
  public smallLine2ChartLabels = chartData.smallLine2ChartLabels;
  public smallLine2ChartOptions = chartData.smallLine2ChartOptions;
  public smallLine2ChartColors = chartData.smallLine2ChartColors;
  public smallLine2ChartLegend = chartData.smallLine2ChartLegend;
  public smallLine2ChartType = chartData.smallLine2ChartType;

  // lineChart
  public smallLine3ChartData = chartData.smallLine3ChartData;
  public smallLine3ChartLabels = chartData.smallLine3ChartLabels;
  public smallLine3ChartOptions = chartData.smallLine3ChartOptions;
  public smallLine3ChartColors = chartData.smallLine3ChartColors;
  public smallLine3ChartLegend = chartData.smallLine3ChartLegend;
  public smallLine3ChartType = chartData.smallLine3ChartType;

  // lineChart
  public smallLine4ChartData = chartData.smallLine4ChartData;
  public smallLine4ChartLabels = chartData.smallLine4ChartLabels;
  public smallLine4ChartOptions = chartData.smallLine4ChartOptions;
  public smallLine4ChartColors = chartData.smallLine4ChartColors;
  public smallLine4ChartLegend = chartData.smallLine4ChartLegend;
  public smallLine4ChartType = chartData.smallLine4ChartType;

  public chart3 = chartData.chart3;



  // events
  public chartClicked(e: any): void {
  }
  public chartHovered(e: any): void {
  }

  async ngOnInit() {
    
    this.findUserByEmail()
    this.findUserByEmaill()
    this.getallusers()
    
    

    this.promoService.getPromo().subscribe((res: any) => {
      this.count = res.length
    })

    this.userService.getUsers().subscribe(res => {
      console.log(res)
      this.listacheteur = res
        .filter((el: any) => el.roles[0]?.name == 'CLIENT')
      this.count1 = this.listacheteur.length
    })
    this.userService.getUsers().subscribe(res => {
      console.log(res)
      this.listvendeur = res
        .filter((el: any) => el.roles[0]?.name == 'VENDEUR')
      this.count2 = this.listvendeur.length
    })

   
  }
 

  getallpromoss() {
    this.promoService.getPromo().subscribe(res => {
      
      this.listpromo = res
      console.log("listpromo", this.listpromo)
    })
  }

  getallpromos(id:any) {
    console.log("id of get All Promo ",id)
    this.promoService.getPromoBy(id).subscribe(res => {
      this.listpromo = res
      console.log("listpromo",this.listpromo)
    })
  }
   findUserByEmaill() {
    this.userService.findUser(this.email).subscribe(async(res:any) => {
     
      this.user=res
      console.log("role of user",this.testRole)
     
      if(this.testRole=="ADMIN")
      
       this.getallpromoss()
      else
      this.getallpromos(this.user.id)
      
    })
   
  }
   findUserByEmail() {
    this.userService.findUser(this.email).subscribe(res => {
       this.user = res
       this.promoService.getCommandevendeur(res.id).subscribe(async res => {
     
         this.ordes = res
         this.count4 = this.ordes.length
         console.log("orders of user ", this.ordes)
         
       })
    })
   
  }

  
  getallusers() {

    this.userService.getUsers().subscribe(res => {
      console.log(res)
      this.user = res
      this.count5 = res.length
      console.log("list user", this.user)
    })
  }

}
