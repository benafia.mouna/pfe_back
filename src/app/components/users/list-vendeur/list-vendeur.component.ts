import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/service/userService';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-vendeur',
  templateUrl: './list-vendeur.component.html',
  styleUrls: ['./list-vendeur.component.scss']
})
export class ListVendeurComponent implements OnInit {

  listuser : any;
  listvendeur : any;
    constructor(private userService : UserService) { }
  
    ngOnInit() {
      this.getAllvendeur()
      this.getallusers()
    }
    getAllvendeur () {
      this.userService.getUsers().subscribe(res => {
        console.log(res)
        this.listvendeur = res
         .filter((el:any)=>el.roles[0] ?.name=='VENDEUR')
      console.log("listvendeur",this.listvendeur)
      })
    }
    public onOpenModal(userId:number): void { 
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.userService.deleteUser(userId).subscribe(
            (reponse: void) => {
              console.log('c est fait avec sucess !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', userId)
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              )
              console.log(reponse);
              this.getallusers();
            },
          );
          
        }
      })
      
    }
    onDeleteUser(userId:number ):void{ 
      console.log(userId);
      
      }
      getallusers() {

        this.userService.getUsers().subscribe(res => {
          console.log(res)
          this.listuser = res
          console.log("list user", this.listuser)
        })
      }
  
  }