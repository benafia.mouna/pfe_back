import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListVendeurComponent } from './list-vendeur.component';

describe('ListVendeurComponent', () => {
  let component: ListVendeurComponent;
  let fixture: ComponentFixture<ListVendeurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListVendeurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListVendeurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
