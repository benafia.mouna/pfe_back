import { Component, OnInit } from '@angular/core';
import { userListDB } from 'src/app/shared/tables/list-users';
import { User } from 'src/model/user';
import { UserService } from 'src/service/userService';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {
  public user_list = []
  listuser: any
  users: any;
  user:any
  public editUser: User ;
  public deletUser : any;
  constructor(private userservice: UserService , private userService : UserService) {
     this.user_list = userListDB.list_user;
  }
  ngOnInit() {
    this.getallusers()
  }
  getallusers() {

    this.userservice.getUsers().subscribe(res => {
      console.log(res)
      this.listuser = res
      console.log("list user", this.listuser)
    })
  }
  public onOpenModal(userId:number): void { 
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.userService.deleteUser(userId).subscribe(
          (reponse: void) => {
            console.log('c est fait avec sucess !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', userId)
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            console.log(reponse);
            this.getallusers();
          },
        );
        
      }
    })
    
  }
  isvendeur(){
    return this.user?.roles[0]?.name=="VENDEUR"? true:false
  }
  
  isclient(){
    return this.user?.roles[0]?.name=="CLIENT"? true:false
  }
  
  
onDeleteUser(userId:number ):void{ 
console.log(userId);

}

}

