import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { RoleService } from 'src/service/roleService';
import { UserService } from 'src/service/userService';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-acheteur',
  templateUrl: './list-acheteur.component.html',
  styleUrls: ['./list-acheteur.component.scss']
})
export class ListAcheteurComponent implements OnInit {
listuser : any
deletUser:any
listacheteur : any
  constructor(private userService : UserService) { }

  ngOnInit(): void {
  this.getAllacheteur()
  }
  getAllacheteur () {
    this.userService.getUsers().subscribe(res => {
      console.log(res)
      this.listacheteur = res
      .filter((el:any)=>el.roles[0] ?.name=='CLIENT')
    console.log("listacheteur",this.listacheteur)
    })
  }
  public onOpenModal(userId:number): void { 
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.userService.deleteUser(userId).subscribe(
          (reponse: void) => {
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            console.log(reponse);
            this.getAllacheteur();
          },
        );
        
      }
    })
    
  }
  
  
  
onDeleteUser(userId:number ):void{ 
console.log(userId);

}

  getallusers() {

    this.userService.getUsers().subscribe(res => {
      console.log(res)
      this.listuser = res
      console.log("list user", this.listuser)
    })
  }

}


