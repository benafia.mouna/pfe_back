import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { Ng2SmartTableModule } from 'ng2-smart-table';
import { UsersRoutingModule } from './users-routing.module';
import { ListUserComponent } from './list-user/list-user.component';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ListAcheteurComponent } from './list-acheteur/list-acheteur.component';
import { ListVendeurComponent } from './list-vendeur/list-vendeur.component';
import { DetailvendeurComponent } from './detailvendeur/detailvendeur.component';


@NgModule({
  declarations: [ListUserComponent ,ListAcheteurComponent,ListVendeurComponent,DetailvendeurComponent],
  imports: [
    CommonModule,
    NgbModule,
    Ng2SmartTableModule,
    ReactiveFormsModule,
    UsersRoutingModule,
    FormsModule
  ]
})
export class UsersModule { }
