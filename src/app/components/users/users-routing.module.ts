import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListUserComponent } from './list-user/list-user.component';
import { ListAcheteurComponent } from './list-acheteur/list-acheteur.component';
import { ListVendeurComponent } from './list-vendeur/list-vendeur.component';
import { DetailvendeurComponent } from './detailvendeur/detailvendeur.component';


const routes: Routes = [

  {
    path: '',
    children: [
      {
        path: 'list-user',
        component: ListUserComponent,
        data: {
          title: "User List",
          breadcrumb: "User List"
        }
      },
      {
        path: 'list-acheteur',
        component: ListAcheteurComponent,
        data: {
          title: "Buyer List",
          breadcrumb: "Buyer List"
        }
      },
      {
        path: 'list-vendeur',
        component: ListVendeurComponent,
        data: {
          title: "Seller List",
          breadcrumb: "Seller List"
        }
      },
      {
        path: 'detail/:id',
        component: DetailvendeurComponent,
      
      },
    
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
