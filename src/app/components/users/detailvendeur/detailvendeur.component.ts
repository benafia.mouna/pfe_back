import { Component, OnInit } from '@angular/core';
import { User } from 'src/model/user';
import { UserService } from 'src/service/userService';

@Component({
  selector: 'app-detailvendeur',
  templateUrl: './detailvendeur.component.html',
  styleUrls: ['./detailvendeur.component.scss']
})
export class DetailvendeurComponent implements OnInit {
  listuser: any;
  user: User;
  email= localStorage.getItem("email")
  fileToUpload: Array<File> = [];
  url: any;
  constructor(private userservice : UserService) { }

  ngOnInit(): void {
    this.getallusers()
    this.findUserByEmail()
  }
  getallusers() {

    this.userservice.getUsers().subscribe(res => {
      console.log(res)
      this.listuser = res
    })
  }
  findUserByEmail() {
    console.log("email",this.email)
    this.userservice.findUser(this.email).subscribe(res => {
      this.user=res
    })
  }
  isentreprise(){
    return this.user?.type=="is_Entreprise"? true:false
  }
  isvendeur() {
    return this.user?.roles[0]?.name == "VENDEUR" ? true : false
  }
  isparticulier(){
    return this.user?.type=="is_Particulier"? true:false
  }
  handleFileInput(files: any) {
    this.fileToUpload = <Array<File>>files.target.files;
    console.log("file To Upload " + this.fileToUpload);
  }
  
}
