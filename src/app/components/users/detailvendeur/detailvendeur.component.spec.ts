import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailvendeurComponent } from './detailvendeur.component';

describe('DetailvendeurComponent', () => {
  let component: DetailvendeurComponent;
  let fixture: ComponentFixture<DetailvendeurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailvendeurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailvendeurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
