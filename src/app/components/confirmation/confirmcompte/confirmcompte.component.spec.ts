import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmcompteComponent } from './confirmcompte.component';

describe('ConfirmcompteComponent', () => {
  let component: ConfirmcompteComponent;
  let fixture: ComponentFixture<ConfirmcompteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmcompteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmcompteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
