import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfirmationRoutingModule } from './confirmation-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ConfirmationRoutingModule
  ]
})
export class ConfirmationModule { }
