import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from '../auth/login/login.component';
import {ConfirmcompteComponent} from './confirmcompte/confirmcompte.component';
import {EmailvalidComponent} from './emailvalid/emailvalid.component';

const routes: Routes = [
  {
    path: 'confirm',
    component: ConfirmcompteComponent,
  },
  {
    path: 'valid',
    component: EmailvalidComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfirmationRoutingModule { }
