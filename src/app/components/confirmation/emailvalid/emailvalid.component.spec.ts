import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailvalidComponent } from './emailvalid.component';

describe('EmailvalidComponent', () => {
  let component: EmailvalidComponent;
  let fixture: ComponentFixture<EmailvalidComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmailvalidComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailvalidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
