import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { content } from './shared/routes/content-routes';
import { ContentLayoutComponent } from './shared/layout/content-layout/content-layout.component';
import { LoginComponent } from './components/auth/login/login.component';
import { ForgotpasswordComponent } from './components/auth/forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './components/auth/resetpassword/resetpassword.component';
import { ErrorComponent } from './components/error/error.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth/login',
    pathMatch: 'full'
  },
  {
    path: '',
    component: ContentLayoutComponent,canActivate:[AuthGuard],
    children: content
  },
  {
    path: 'auth/login',
    component: LoginComponent,
  },
  {
    path: 'forget',
    component: ForgotpasswordComponent,

  },
  {
    path: 'reset/:resetLink',
    component: ResetpasswordComponent,

  },
 
  {
    path: '',
    loadChildren: () => import('../../src/app/components/confirmation/confirmation.module').then(m => m.ConfirmationModule),
  },
  {
    path: '404',
    component: ErrorComponent,

  },
  {
    path: '**',
    component: ErrorComponent,

  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    relativeLinkResolution: 'legacy'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
